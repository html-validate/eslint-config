# @html-validate/eslint-config

> HTML-Validate ESLint [shareable](http://eslint.org/docs/developer-guide/shareable-configs.html) config.

## Install

```
npm install --save-dev @html-validate/eslint-config
```

## Usage

In your `.eslintrc.js` file:

```js
require("@html-validate/eslint-config/patch/modern-module-resolution");

module.exports = {
  extends: ["@html-validate"],

  overrides: [
    {
      files: "*.ts",
      extends: ["@html-validate/typescript"],
    },
    {
      files: "*.spec.[jt]s",
      extends: ["@html-validate/jest"],
    },
  ],
};
```

## CLI

To automatically write configuration use:

    npx @html-validate/eslint-config --write

To check if configuration is up-to-date use:

    npx @html-validate/eslint-config --check

The tool will autodetect support for:

- AngularJS
- Cypress
- Jest
- Protractor
- Typescript
- Vue

Use `--enable-FEATURE` or `--disable-FEATURE` to manually enable or disable.

## Typescript with type information

Use `--enable-typeinfo` (enabled by default) or manually configure with:

```diff
   overrides: [
     {
       files: "*.ts",
-       extends: ["@html-validate/typescript"],
+       parserOptions: {
+         tsconfigRootDir: __dirname,
+         project: ["./tsconfig.json"],
+       },
+       extends: ["@html-validate/typescript", "@html-validate/typescript-typeinfo"],
     },
   ],
```
