This is an internal build of Prettier used for unit-testing.
It is needed until the Jest ecosystem can natively handle ESM (preferably without using experimental flags).

The package is prebundled as CJS with shims to handle ESM features such as `import.meta.url` which Jest can load without issues.
