/* based on tsup implementation: https://github.com/egoist/tsup/blob/dev/assets/cjs_shims.js */

const getImportMetaUrl = () => new URL(`file:${__filename}`).href;
export const importMetaUrl = /* @__PURE__ */ getImportMetaUrl();
