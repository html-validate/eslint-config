import esbuild from "esbuild";

await esbuild.build({
	entryPoints: ["src/index.js"],
	outdir: "dist",
	bundle: true,
	format: "cjs",
	platform: "node",
	target: "node18",
	logLevel: "info",
	define: {
		"import.meta.url": "importMetaUrl",
	},
	inject: ["src/shim.js"],
	supported: {
		"class-field": false,
	},
});
