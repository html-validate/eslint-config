import fs from "fs";
import path from "path";

function readFile(filename: string): string | undefined {
	const filePath = path.join(__dirname, filename);
	if (fs.existsSync(filePath)) {
		return fs.readFileSync(filePath, "utf-8");
	} else {
		return undefined;
	}
}

it("package.json", () => {
	expect.assertions(2);
	const pkg = JSON.parse(readFile("package.json"));
	expect(pkg.scripts).toMatchSnapshot();
	expect(Object.keys(pkg.devDependencies)).toMatchSnapshot();
});

it(".eslintrc.cjs", () => {
	expect.assertions(1);
	expect(readFile(".eslintrc.cjs")).toMatchSnapshot();
});

it(".eslintignore", () => {
	expect.assertions(1);
	expect(readFile(".eslintignore")).toMatchSnapshot();
});
