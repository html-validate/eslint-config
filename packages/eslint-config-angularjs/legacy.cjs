module.exports = {
	extends: ["plugin:angular/johnpapa", "plugin:angular/bestpractices"],
	rules: {
		"angular/controller-as-vm": ["error", "$ctrl"],
		"angular/file-name": "off",
		"angular/no-service-method": "off",
	},
};
