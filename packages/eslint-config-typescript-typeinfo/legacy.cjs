module.exports = {
	extends: [
		"plugin:@typescript-eslint/strict-type-checked",
		"plugin:@typescript-eslint/stylistic-type-checked",
	],

	rules: {
		/* prefer T[] for simple types, Array<T> for complex types (unions, etc) */
		"@typescript-eslint/array-type": ["error", { default: "array-simple" }],

		/* allow getters which returns a literal */
		"@typescript-eslint/class-literal-property-style": "off",

		/* no-explicit-any is enabled and for now this rule is a bit to tedious to
		 * actually help */
		"@typescript-eslint/no-unsafe-member-access": "off",

		"@typescript-eslint/no-inferrable-types": "off",

		/* allow function(this: void, ...) */
		"@typescript-eslint/no-invalid-void-type": [
			"error",
			{
				allowAsThisParameter: true,
			},
		],

		/* prefer interface over type = { .. } */
		"@typescript-eslint/consistent-type-definitions": ["error", "interface"],

		/* enforce usage of "type" with export/import */
		"@typescript-eslint/consistent-type-exports": [
			"error",
			{
				fixMixedExportsWithInlineTypeSpecifier: true,
			},
		],
		"@typescript-eslint/consistent-type-imports": [
			"error",
			{
				fixStyle: "inline-type-imports",
			},
		],

		/* allow constructs such as `unknown | null`, while `unknown` does override
		 * `null` it can still serve as a self-documenting type to signal that
		 * `null` has a special meaning. It also helps when the type is to be
		 * replaced with a better type later. */
		"@typescript-eslint/no-redundant-type-constituents": "off",

		"@typescript-eslint/no-unused-vars": [
			"error",
			{
				ignoreRestSiblings: true,
				argsIgnorePattern: "^_",
			},
		],

		/* allow overloading only if the parameters have different names */
		"@typescript-eslint/unified-signatures": [
			"error",
			{
				ignoreDifferentlyNamedParameters: true,
			},
		],
	},
};
