module.exports = {
	parser: "@typescript-eslint/parser",

	plugins: ["@typescript-eslint", "tsdoc"],

	extends: [
		"plugin:@typescript-eslint/strict",
		"plugin:@typescript-eslint/stylistic",
		"plugin:import/typescript",
	],

	rules: {
		/* typescript handles the return types */
		"consistent-return": "off",

		"tsdoc/syntax": "error",

		/* prefer T[] for simple types, Array<T> for complex types (unions, etc) */
		"@typescript-eslint/array-type": ["error", { default: "array-simple" }],

		/* allow getters which returns a literal */
		"@typescript-eslint/class-literal-property-style": "off",

		"@typescript-eslint/explicit-function-return-type": [
			"error",
			{
				allowExpressions: true,
			},
		],
		"@typescript-eslint/explicit-member-accessibility": "error",
		"@typescript-eslint/no-inferrable-types": "off",

		/* allow function(this: void, ...) */
		"@typescript-eslint/no-invalid-void-type": [
			"error",
			{
				allowAsThisParameter: true,
			},
		],

		/* allow constructs such as `unknown | null`, while `unknown` does override
		 * `null` it can still serve as a self-documenting type to signal that
		 * `null` has a special meaning. It also helps when the type is to be
		 * replaced with a better type later. */
		"@typescript-eslint/no-redundant-type-constituents": "off",

		"@typescript-eslint/no-unused-vars": [
			"error",
			{
				ignoreRestSiblings: true,
				argsIgnorePattern: "^_",
			},
		],

		/* allow overloading only if the parameters have different names */
		"@typescript-eslint/unified-signatures": [
			"error",
			{
				ignoreDifferentlyNamedParameters: true,
			},
		],
	},
};
