module.exports = {
	env: {
		jest: true,
	},

	plugins: ["jest"],

	extends: ["plugin:jest/recommended", "plugin:jest/style"],

	rules: {
		"jest/consistent-test-it": ["error", { fn: "it" }],
		"jest/no-disabled-tests": "warn",
		"jest/no-duplicate-hooks": "error",
		"jest/no-focused-tests": "warn",
		"jest/no-test-prefixes": "warn",
		"jest/prefer-expect-assertions": "error",
		"jest/prefer-hooks-on-top": "error",
		"jest/prefer-todo": "error",
		"jest/no-restricted-matchers": [
			"error",
			{
				resolves: "Use `expect(await promise)` instead.",
			},
		],

		/* allow ! assertions in testcases */
		"@typescript-eslint/no-non-null-assertion": "off",

		/* common in testcases, kind of useless */
		"sonarjs/no-duplicate-string": "off",

		/* yields error way to early (considering jest is full of describe with it and this rule considers even arrow functions) */
		"sonarjs/no-nested-functions": "off",

		/* jest uses @jest-* tags for per-file configuration */
		"tsdoc/syntax": "off",
	},
};
