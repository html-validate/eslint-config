import { resolveConfig, format } from "internal-prettier";

module.exports = { resolveConfig, format };
module.exports.default = module.exports;
