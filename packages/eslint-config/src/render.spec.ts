import path from "node:path";
import vm from "node:vm";
import url from "node:url";
import { renderNunjucks, Features, renderPassthru } from "./render";

/* poor mans transpiler */
function transpile(source: string): string {
	return source
		.replace(/^import ([^ ]+) from "([^"]+)";$/gm, 'const $1 = require("$2");')
		.replace(/^import {([^}]+)} from "([^"]+)";$/gm, 'const {$1} = require("$2");')
		.replace(/import.meta.url/gm, "importMetaUrl")
		.replace(/^export default /m, "module.exports = ");
}

function compile(source: string): unknown {
	const transpiled = transpile(source);
	function mockRequire(
		name: string,
	): string | typeof import("node:path") | typeof import("node:url") {
		switch (name) {
			case "path":
			case "node:path":
				return path;
			case "url":
			case "node:url":
				return url;
			default:
				return name;
		}
	}
	mockRequire.resolve = (name: string): string => name;
	const module = { exports: undefined as unknown };
	const dirname = "/path/to/project";
	const importMetaUrl = url.pathToFileURL("/path/to/project/index.ts");
	vm.runInThisContext(`((require, module, __dirname, importMetaUrl) => {${transpiled}})`)(
		mockRequire,
		module,
		dirname,
		importMetaUrl,
	);
	return module.exports;
}

describe("renderPasstru", () => {
	const filename = path.join(__dirname, "../template/eslintignore");

	it("should pass source as-is", async () => {
		expect.assertions(1);
		const result = await renderPassthru(filename);
		expect(result).toMatchSnapshot();
	});
});

describe("renderNunjucks (legacy)", () => {
	const filename = path.join(__dirname, "../template/eslintrc.js.njk");
	const target = ".eslintrc.js";

	it("should render base template", async () => {
		expect.assertions(2);
		const context: Features = {
			angularjs: false,
			cypress: false,
			jest: false,
			protractor: false,
			typeinfo: false,
			typescript: false,
			vue: false,
		};
		const result = await renderNunjucks(filename, target, context);
		expect(result).toMatchSnapshot();
		expect(() => compile(result)).not.toThrow();
	});

	it("should render template with typescript", async () => {
		expect.assertions(2);
		const context: Features = {
			angularjs: false,
			cypress: false,
			jest: false,
			protractor: false,
			typeinfo: false,
			typescript: true,
			vue: false,
		};
		const result = await renderNunjucks(filename, target, context);
		expect(result).toMatchSnapshot();
		expect(() => compile(result)).not.toThrow();
	});

	it("should render template with typescript and typeinfo", async () => {
		expect.assertions(2);
		const context: Features = {
			angularjs: false,
			cypress: false,
			jest: false,
			protractor: false,
			typeinfo: "tsconfig.json",
			typescript: true,
			vue: false,
		};
		const result = await renderNunjucks(filename, target, context);
		expect(result).toMatchSnapshot();
		expect(() => compile(result)).not.toThrow();
	});

	it("should render template with typescript and typeinfo (subdirectory)", async () => {
		expect.assertions(2);
		const context: Features = {
			angularjs: false,
			cypress: false,
			jest: false,
			protractor: false,
			typeinfo: "src/tsconfig.json",
			typescript: true,
			vue: false,
		};
		const result = await renderNunjucks(filename, target, context);
		expect(result).toMatchSnapshot();
		expect(() => compile(result)).not.toThrow();
	});

	it("should render template with jest", async () => {
		expect.assertions(2);
		const context: Features = {
			angularjs: false,
			cypress: false,
			jest: true,
			protractor: false,
			typeinfo: false,
			typescript: false,
			vue: false,
		};
		const result = await renderNunjucks(filename, target, context);
		expect(result).toMatchSnapshot();
		expect(() => compile(result)).not.toThrow();
	});

	it("should render template with jest (angularjs support)", async () => {
		expect.assertions(2);
		const context: Features = {
			angularjs: true,
			cypress: false,
			jest: true,
			protractor: false,
			typeinfo: false,
			typescript: false,
			vue: false,
		};
		const result = await renderNunjucks(filename, target, context);
		expect(result).toMatchSnapshot();
		expect(() => compile(result)).not.toThrow();
	});

	it("should render template with cypress", async () => {
		expect.assertions(2);
		const context: Features = {
			angularjs: false,
			cypress: true,
			jest: false,
			protractor: false,
			typeinfo: false,
			typescript: false,
			vue: false,
		};
		const result = await renderNunjucks(filename, target, context);
		expect(result).toMatchSnapshot();
		expect(() => compile(result)).not.toThrow();
	});

	it("should render template with protractor", async () => {
		expect.assertions(2);
		const context: Features = {
			angularjs: false,
			cypress: false,
			jest: false,
			protractor: true,
			typeinfo: false,
			typescript: false,
			vue: false,
		};
		const result = await renderNunjucks(filename, target, context);
		expect(result).toMatchSnapshot();
		expect(() => compile(result)).not.toThrow();
	});

	it("should render template with angularjs", async () => {
		expect.assertions(2);
		const context: Features = {
			angularjs: true,
			cypress: false,
			jest: false,
			protractor: false,
			typeinfo: false,
			typescript: false,
			vue: false,
		};
		const result = await renderNunjucks(filename, target, context);
		expect(result).toMatchSnapshot();
		expect(() => compile(result)).not.toThrow();
	});

	it("should render template with vue", async () => {
		expect.assertions(2);
		const context: Features = {
			angularjs: false,
			cypress: false,
			jest: false,
			protractor: false,
			typeinfo: false,
			typescript: false,
			vue: true,
		};
		const result = await renderNunjucks(filename, target, context);
		expect(result).toMatchSnapshot();
		expect(() => compile(result)).not.toThrow();
	});

	it("should render full template", async () => {
		expect.assertions(2);
		const context: Features = {
			angularjs: true,
			cypress: true,
			jest: true,
			protractor: true,
			typeinfo: false,
			typescript: true,
			vue: true,
		};
		const result = await renderNunjucks(filename, target, context);
		expect(result).toMatchSnapshot();
		expect(() => compile(result)).not.toThrow();
	});
});

describe("renderNunjucks (flat)", () => {
	const filename = path.join(__dirname, "../template/eslint.config.mjs.njk");
	const target = "eslint.config.mjs";

	it("should render base template", async () => {
		expect.assertions(2);
		const context: Features = {
			angularjs: false,
			cypress: false,
			jest: false,
			protractor: false,
			typeinfo: false,
			typescript: false,
			vue: false,
		};
		const result = await renderNunjucks(filename, target, context);
		expect(result).toMatchSnapshot();
		expect(() => compile(result)).not.toThrow();
	});

	it("should render template with typescript", async () => {
		expect.assertions(2);
		const context: Features = {
			angularjs: false,
			cypress: false,
			jest: false,
			protractor: false,
			typeinfo: false,
			typescript: true,
			vue: false,
		};
		const result = await renderNunjucks(filename, target, context);
		expect(result).toMatchSnapshot();
		expect(() => compile(result)).not.toThrow();
	});

	it("should render template with typescript and typeinfo", async () => {
		expect.assertions(2);
		const context: Features = {
			angularjs: false,
			cypress: false,
			jest: false,
			protractor: false,
			typeinfo: "tsconfig.json",
			typescript: true,
			vue: false,
		};
		const result = await renderNunjucks(filename, target, context);
		expect(result).toMatchSnapshot();
		expect(() => compile(result)).not.toThrow();
	});

	it("should render template with typescript and typeinfo (subdirectory)", async () => {
		expect.assertions(2);
		const context: Features = {
			angularjs: false,
			cypress: false,
			jest: false,
			protractor: false,
			typeinfo: "src/tsconfig.json",
			typescript: true,
			vue: false,
		};
		const result = await renderNunjucks(filename, target, context);
		expect(result).toMatchSnapshot();
		expect(() => compile(result)).not.toThrow();
	});

	it("should render template with jest", async () => {
		expect.assertions(2);
		const context: Features = {
			angularjs: false,
			cypress: false,
			jest: true,
			protractor: false,
			typeinfo: false,
			typescript: false,
			vue: false,
		};
		const result = await renderNunjucks(filename, target, context);
		expect(result).toMatchSnapshot();
		expect(() => compile(result)).not.toThrow();
	});

	it("should render template with jest (angularjs support)", async () => {
		expect.assertions(2);
		const context: Features = {
			angularjs: true,
			cypress: false,
			jest: true,
			protractor: false,
			typeinfo: false,
			typescript: false,
			vue: false,
		};
		const result = await renderNunjucks(filename, target, context);
		expect(result).toMatchSnapshot();
		expect(() => compile(result)).not.toThrow();
	});

	it("should render template with cypress", async () => {
		expect.assertions(2);
		const context: Features = {
			angularjs: false,
			cypress: true,
			jest: false,
			protractor: false,
			typeinfo: false,
			typescript: false,
			vue: false,
		};
		const result = await renderNunjucks(filename, target, context);
		expect(result).toMatchSnapshot();
		expect(() => compile(result)).not.toThrow();
	});

	it("should render template with protractor", async () => {
		expect.assertions(2);
		const context: Features = {
			angularjs: false,
			cypress: false,
			jest: false,
			protractor: true,
			typeinfo: false,
			typescript: false,
			vue: false,
		};
		const result = await renderNunjucks(filename, target, context);
		expect(result).toMatchSnapshot();
		expect(() => compile(result)).not.toThrow();
	});

	it("should render template with angularjs", async () => {
		expect.assertions(2);
		const context: Features = {
			angularjs: true,
			cypress: false,
			jest: false,
			protractor: false,
			typeinfo: false,
			typescript: false,
			vue: false,
		};
		const result = await renderNunjucks(filename, target, context);
		expect(result).toMatchSnapshot();
		expect(() => compile(result)).not.toThrow();
	});

	it("should render template with vue", async () => {
		expect.assertions(2);
		const context: Features = {
			angularjs: false,
			cypress: false,
			jest: false,
			protractor: false,
			typeinfo: false,
			typescript: false,
			vue: true,
		};
		const result = await renderNunjucks(filename, target, context);
		expect(result).toMatchSnapshot();
		expect(() => compile(result)).not.toThrow();
	});

	it("should render full template", async () => {
		expect.assertions(2);
		const context: Features = {
			angularjs: true,
			cypress: true,
			jest: true,
			protractor: true,
			typeinfo: false,
			typescript: true,
			vue: true,
		};
		const result = await renderNunjucks(filename, target, context);
		expect(result).toMatchSnapshot();
		expect(() => compile(result)).not.toThrow();
	});
});
