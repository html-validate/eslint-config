/* eslint-disable n/no-process-exit -- cli script need to exit with exit code 1 on failure */
/* eslint-disable camelcase -- child_process and argparse (modeled after python) uses snake_case */
import fs from "fs";
import child_process from "child_process";
import { promisify } from "util";
import path from "path";
import { ArgumentParser } from "argparse";
import { findUp } from "find-up";
import { renderNunjucks, Renderer, renderPassthru, Features } from "./render";

interface Options {
	rootDir: string;
	flat: boolean;
	dryRun: boolean;
	changes: number;
}

interface Args {
	mode: "check" | "write";
	flat: boolean;
	angularjs: null | boolean;
	cypress: null | boolean;
	jest: null | boolean;
	protractor: null | boolean;
	typeinfo: null | false | string;
	typescript: null | boolean;
	vue: null | boolean;
}

/* eslint-disable-next-line @typescript-eslint/no-require-imports -- should require in npm package not bundled */
const pkg = require("../package.json");

const exec = promisify(child_process.exec);
const templateDir = path.join(__dirname, "../template");
const PACKAGE_JSON = "package.json";

async function getRootDir(): Promise<string> {
	const pkgFile = await findUp(PACKAGE_JSON);
	if (!pkgFile) {
		console.error("Failed to locate project root");
		process.exit(1);
	}
	return path.dirname(pkgFile);
}

function resolveTemplate(originalFilename: string): [string, Renderer] {
	const filename = originalFilename
		.replace(/^\./, "") /* strip leading dot */
		.replace(/\.cjs$/, ".js"); /* replace .cjs with .js */
	const literal = path.join(templateDir, filename);
	if (fs.existsSync(literal)) {
		return [literal, renderPassthru];
	}

	const njk = path.join(templateDir, `${filename}.njk`);
	if (fs.existsSync(njk)) {
		return [njk, renderNunjucks];
	}

	throw new Error(`Failed to locate template "${originalFilename}" in "${templateDir}"`);
}

/**
 * Ensure a file is missing
 */
async function ensureFileRemoved(filename: string, options: Options): Promise<void> {
	if (!fs.existsSync(filename)) {
		return;
	}
	options.changes++;
	if (options.dryRun) {
		console.log(`${filename} needs removal`);
	} else {
		fs.unlinkSync(filename);
		console.log(`${filename} removed`);
	}
}

/**
 * Ensures a file matches template.
 */
async function ensureFileExists(
	filename: string,
	options: Options,
	features: Features,
): Promise<void> {
	const [src, renderer] = resolveTemplate(filename);
	const dst = path.join(options.rootDir, filename);
	const expected = await renderer(src, dst, features);

	/* if the file exists compare the file with the template */
	if (fs.existsSync(dst)) {
		const actual = fs.readFileSync(dst, "utf-8");
		if (actual === expected) {
			return;
		}
	}

	options.changes++;

	if (options.dryRun) {
		console.log(`${filename} - needs update`);
	} else {
		fs.writeFileSync(dst, expected);
		console.log(`${filename} written`);
	}
}

/**
 * Ensures package.json have dependency.
 *
 * @param name - Name of package which should be installed.
 */
async function ensureDependencyRemoved(name: string, options: Options): Promise<void> {
	const dst = path.join(options.rootDir, PACKAGE_JSON);
	const data = fs.readFileSync(dst, "utf-8");
	const pkg = JSON.parse(data);
	const dependencies = new Set([
		...Object.keys(pkg.dependencies || {}),
		...Object.keys(pkg.devDependencies || {}),
		...Object.keys(pkg.peerDependencies || {}),
	]);
	if (!dependencies.has(name)) {
		return;
	}

	options.changes++;

	if (options.dryRun) {
		console.log(`package.json - ${name} dependency is present when it shouldn't`);
	} else {
		console.log(`package.json - removing dependency "${name}"`);
		try {
			await exec(`npm rm --save-dev ${name}`);
		} /* eslint-disable-line @typescript-eslint/no-explicit-any -- unknown type */ catch (err: any) {
			console.error(err.stdout);
			console.error(err.stderr);
		}
	}
}

/**
 * Ensures package.json have dependency.
 *
 * @param name - Name of package which should be installed.
 */
async function ensureDependencyExists(name: string, options: Options): Promise<void> {
	const dst = path.join(options.rootDir, PACKAGE_JSON);
	const data = fs.readFileSync(dst, "utf-8");
	const pkg = JSON.parse(data);
	const dependencies = new Set([
		...Object.keys(pkg.dependencies || {}),
		...Object.keys(pkg.devDependencies || {}),
		...Object.keys(pkg.peerDependencies || {}),
	]);
	if (dependencies.has(name)) {
		return;
	}

	options.changes++;

	if (options.dryRun) {
		console.log(`package.json - ${name} dependency is missing`);
	} else {
		console.log(`package.json - installing dependency "${name}"`);
		try {
			await exec(`npm install --save-dev ${name}`);
		} /* eslint-disable-line @typescript-eslint/no-explicit-any -- unknown type */ catch (err: any) {
			console.error(err.stdout);
			console.error(err.stderr);
		}
	}
}

/**
 * Ensures package.json have given script.
 *
 * @param script - Name of script.
 * @param command - Command the script should be set to.
 */
async function ensurePkgScript(script: string, command: string, options: Options): Promise<void> {
	const dst = path.join(options.rootDir, PACKAGE_JSON);
	const data = fs.readFileSync(dst, "utf-8");
	const pkg = JSON.parse(data);

	if (pkg.scripts && pkg.scripts[script] && pkg.scripts[script] === command) {
		return;
	}

	options.changes++;

	if (options.dryRun) {
		console.log(`package.json - ${script} script needs update`);
	} else {
		if (!pkg.scripts) {
			pkg.scripts = {};
		}
		pkg.scripts[script] = command;
		const output = JSON.stringify(pkg, null, 2);
		fs.writeFileSync(dst, `${output}\n`);
		console.log(`package.json "${script}" script updated`);
	}
}

/* eslint-disable-next-line complexity -- too many if-statements but is probably more readable this way */
async function writeConfig(options: Options, features: Features): Promise<boolean> {
	/* ensure old configuration files is not present */
	await ensureFileRemoved(".eslintrc.json", options);

	/* ensure all files are up-to-date */
	if (options.flat) {
		await ensureFileRemoved(".eslintrc.js", options);
		await ensureFileRemoved(".eslintrc.cjs", options);
		await ensureFileRemoved(".eslintignore", options);
		await ensureFileExists("eslint.config.mjs", options, features);
	} else {
		await ensureFileRemoved("eslint.config.js", options);
		await ensureFileRemoved("eslint.config.mjs", options);
		if (fs.existsSync(path.join(options.rootDir, ".eslintrc.js"))) {
			console.log(`Deprecated: project uses ".eslintrc.js" instead of ".eslintrc.cjs".`);
			await ensureFileExists(".eslintrc.js", options, features);
		} else {
			await ensureFileExists(".eslintrc.cjs", options, features);
		}
		await ensureFileExists(".eslintignore", options, features);
	}

	/* ensure package.json is up-to-date */
	await ensurePkgScript("eslint", "eslint --cache .", options);
	await ensurePkgScript("eslint:fix", "eslint --cache --fix .", options);

	/* ensure dependencies are up-to-date */
	await ensureDependencyRemoved("eslint", options);
	await ensureDependencyExists("@html-validate/eslint-config", options);
	if (features.typeinfo) {
		await ensureDependencyExists("@html-validate/eslint-config-typescript-typeinfo", options);
	}
	if (features.typescript) {
		await ensureDependencyExists("@html-validate/eslint-config-typescript", options);
	}
	if (features.jest) {
		await ensureDependencyExists("@html-validate/eslint-config-jest", options);
	}
	if (features.cypress) {
		await ensureDependencyExists("@html-validate/eslint-config-cypress", options);
	}
	if (features.angularjs) {
		await ensureDependencyExists("@html-validate/eslint-config-angularjs", options);
	}
	if (features.vue) {
		await ensureDependencyExists("@html-validate/eslint-config-vue", options);
	}
	if (features.protractor) {
		await ensureDependencyExists("@html-validate/eslint-config-protractor", options);
	}

	/* non-dryrun is always ok */
	if (!options.dryRun) {
		return true;
	}

	if (options.changes > 0) {
		console.error("The files listed needs updating. Use `--write` to write changes automatically.");
	}

	return options.changes === 0;
}

async function getFeatures(options: Options, args: Args): Promise<Features> {
	const dst = path.join(options.rootDir, PACKAGE_JSON);
	const data = fs.readFileSync(dst, "utf-8");
	const pkg = JSON.parse(data);
	const deps = { ...pkg.dependencies, ...pkg.devDependencies };
	return {
		angularjs: args.angularjs ?? Boolean(deps.angular),
		cypress: args.cypress ?? Boolean(deps.cypress),
		jest: args.jest ?? Boolean(deps.jest),
		protractor: args.protractor ?? Boolean(deps.protractor),
		typeinfo: args.typeinfo ?? (deps.typescript ? "./tsconfig.json" : false),
		typescript: args.typescript ?? Boolean(deps.typescript),
		vue: args.vue ?? Boolean(deps.vue),
	};
}

async function run(args: Args): Promise<never> {
	const rootDir = await getRootDir();
	const options: Options = {
		rootDir,
		flat: args.flat,
		dryRun: args.mode === "check",
		changes: 0,
	};
	const features = await getFeatures(options, args);
	const ok = await writeConfig(options, features);
	process.exit(ok ? 0 : 1);
}

const parser = new ArgumentParser({
	add_help: true,
	description: pkg.description,
});

parser.add_argument("-v", "--version", {
	action: "version",
	version: pkg.version,
});

parser.add_argument("-w", "--write", {
	action: "store_const",
	const: "write",
	dest: "mode",
	help: "Write configuration",
});

parser.add_argument("-c", "--check", {
	action: "store_const",
	const: "check",
	dest: "mode",
	help: "Check if configuration is up-to-date",
});

parser.add_argument("--flat-config", {
	action: "store_true",
	dest: "flat",
	help: "Enable experimental support for ESLint flat configuration",
});

parser.add_argument("--disable-cypress", {
	action: "store_const",
	const: false,
	dest: "cypress",
	help: "Disable Cypress support",
});

parser.add_argument("--disable-protractor", {
	action: "store_const",
	const: false,
	dest: "protractor",
	help: "Disable Protractor support",
});

parser.add_argument("--disable-jest", {
	action: "store_const",
	const: false,
	dest: "jest",
	help: "Disable Jest support",
});

parser.add_argument("--disable-typescript", {
	action: "store_const",
	const: false,
	dest: "typescript",
	help: "Disable typescript support",
});

parser.add_argument("--disable-typeinfo", {
	action: "store_const",
	const: false,
	dest: "typeinfo",
	help: "Disable typescript typeinfo support",
});

parser.add_argument("--disable-angularjs", {
	action: "store_const",
	const: false,
	dest: "angularjs",
	help: "Disable AngularJS support",
});

parser.add_argument("--disable-vue", {
	action: "store_const",
	const: false,
	dest: "vue",
	help: "Disable Vue support",
});

parser.add_argument("--enable-cypress", {
	action: "store_const",
	const: true,
	dest: "cypress",
	help: "Enable Cypress support",
});

parser.add_argument("--enable-protractor", {
	action: "store_const",
	const: true,
	dest: "protractor",
	help: "Enable Protractor support",
});

parser.add_argument("--enable-jest", {
	action: "store_const",
	const: true,
	dest: "jest",
	help: "Enable Jest support",
});

parser.add_argument("--enable-typescript", {
	action: "store_const",
	const: true,
	dest: "typescript",
	help: "Enable typescript support",
});

parser.add_argument("--enable-typeinfo", {
	nargs: "?",
	dest: "typeinfo",
	const: "tsconfig.json",
	metavar: "TSCONFIG",
	help: "Enable typescript typeinfo support",
});

parser.add_argument("--enable-angularjs", {
	action: "store_const",
	const: true,
	dest: "angularjs",
	help: "Enable AngularJS support",
});

parser.add_argument("--enable-vue", {
	action: "store_const",
	const: true,
	dest: "vue",
	help: "Enable Vue support",
});

const args = parser.parse_args();

if (!args.mode) {
	parser.print_help();
	process.exit(1);
}

run(args);
