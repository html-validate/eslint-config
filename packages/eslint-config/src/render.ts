import fs from "fs";
import path from "path";
import nunjucks from "nunjucks";
import prettier from "prettier";

export interface Features {
	angularjs: boolean;
	cypress: boolean;
	jest: boolean;
	protractor: boolean;
	typeinfo: false | string;
	typescript: boolean;
	vue: boolean;
}

export type Renderer = (src: string, dst: string, features: Features) => Promise<string>;

export async function renderPassthru(src: string): Promise<string> {
	return fs.readFileSync(src, "utf-8");
}

/**
 * Render source template.
 *
 * @param src - Template filename
 * @param dst - Destination filename. Used to resolve prettier configuration.
 * @returns rendered template
 */
export async function renderNunjucks(
	src: string,
	dst: string,
	features: Features,
): Promise<string> {
	nunjucks.configure(path.dirname(src), { trimBlocks: true });
	const typeinfo = features.typeinfo
		? {
				rootDir: path.dirname(features.typeinfo),
				tsconfig: `./${path.basename(features.typeinfo)}`,
			}
		: false;
	const context = { ...features, typeinfo };
	const result = nunjucks.render(path.basename(src), context);
	const prettierConfig = await prettier.resolveConfig(dst, {
		editorconfig: true,
	});
	return prettier.format(result, {
		...prettierConfig,
		filepath: dst,
	});
}
