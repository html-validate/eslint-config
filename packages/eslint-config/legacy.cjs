module.exports = {
	env: {
		node: true,
	},

	parserOptions: {
		/* force version, some plugins tries to overwrite this */
		ecmaVersion: 2023,
	},

	settings: {
		"import/resolver": {
			[require.resolve("eslint-import-resolver-node")]: true,
			[require.resolve("eslint-import-resolver-typescript")]: true,
		},
	},

	extends: [
		require.resolve("eslint-config-sidvind/es2017"),
		"plugin:eslint-comments/recommended",
		"plugin:prettier/recommended",
		"plugin:import/errors",
		"plugin:n/recommended-module",
		"plugin:array-func/recommended",
		"plugin:security/recommended-legacy",
		"plugin:sonarjs/recommended-legacy",
	],

	plugins: ["array-func", "prettier", "import", "n", "security", "sonarjs"],

	rules: {
		"eslint-comments/disable-enable-pair": ["error", { allowWholeFile: true }],
		"eslint-comments/require-description": [
			"error",
			{ ignore: ["eslint-enable", "eslint-env", "exported", "global", "globals"] },
		],
		"eslint-comments/no-unused-disable": "error",

		"import/default": "off",
		"import/extensions": ["error", "never", { json: "always" }],
		"import/newline-after-import": "error",
		"import/no-absolute-path": "error",
		"import/no-deprecated": "error",
		"import/no-dynamic-require": "error",
		"import/no-extraneous-dependencies": "error",
		"import/no-mutable-exports": "error",
		"import/no-named-default": "error",
		"import/no-useless-path-segments": "error",
		"import/order": "error",
		"import/no-named-as-default": "error",
		"import/no-named-as-default-member": "error",
		"import/no-duplicates": "error",

		/* this is checked by compiler and without additional configuration does not
		 * work with typescript */
		"n/no-missing-import": "off",

		"security/detect-child-process": "off", // produces more noise than useful errors
		"security/detect-non-literal-fs-filename": "off", // html-validate reads files, don't want to acknowledge all occurrences
		"security/detect-non-literal-regexp": "error",
		"security/detect-non-literal-require": "error",
		"security/detect-object-injection": "off", // produces more noise than useful errors
		"security/detect-unsafe-regex": "error",

		"sonarjs/cognitive-complexity": "off", // already covered by native complexity rule
		"sonarjs/deprecation": "off", // already covered by @typescript-eslint/no-deprecated
		"sonarjs/function-return-type": "off", // overly broad and opinionated, let typescript deal with this
		"sonarjs/no-empty-test-file": "off", // could be useful but it does not handle it.each or similar constructs thus yields more false positives than its worth */
		"sonarjs/no-small-switch": "off", // prefer to use small switches when the intention is to all more cases later
		"sonarjs/no-unused-vars": "off", // already coveredby @typescript-eslint/no-unused-vars
		"sonarjs/unused-import": "off", // already covered by @typescript-eslint/no-unused-vars
		"sonarjs/prefer-nullish-coalescing": "off", // requires typescript and strictNullChecks, which is sane, but we also use @typescript-eslint/prefer-nullish-coalescing so this becomes redundant
		"sonarjs/prefer-single-boolean-return": "off", // prefer to use multiple returns even for booleans (looks better and can yield performance increase
		"sonarjs/no-control-regex": "off", // already covered by no-control-regexp
		"sonarjs/prefer-regexp-exec": "off", // prefer @typescript-eslint/prefer-regexp-exec
		"sonarjs/todo-tag": "off", // want to be able to leave todo tasks

		"consistent-this": "off",
		"no-console": "warn",
		"no-dupe-class-members": "off",
		"no-undef": "off",
		"no-unused-vars": [
			"error",
			{
				ignoreRestSiblings: true,
				argsIgnorePattern: "^_",
			},
		],
		"object-shorthand": "error",
		"prettier/prettier": "warn",
		strict: "off",
	},
};
