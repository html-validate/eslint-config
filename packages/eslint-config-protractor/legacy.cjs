module.exports = {
	env: {
		jasmine: true,
		protractor: true,
	},

	plugins: ["jasmine", "protractor"],

	extends: ["plugin:jasmine/recommended", "plugin:protractor/recommended"],

	rules: {
		"jasmine/new-line-before-expect": "off",
		"jasmine/no-disabled-tests": "warn",
		"jasmine/no-focused-tests": "warn",
		"jasmine/no-spec-dupes": ["error", "branch"],
	},
};
