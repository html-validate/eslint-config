module.exports = {
	extends: [
		"plugin:vue/recommended",

		/* apply prettier again because the order the extends are resolved otherwise
		 * casues the conflicting vue rules to be renabled by the vue preset */
		"plugin:prettier/recommended",
	],
};
