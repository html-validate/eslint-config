import { fileURLToPath } from "node:url";
import path from "node:path";
import { FlatCompat } from "@eslint/eslintrc";
import legacyConfig from "./legacy.cjs";

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

const compat = new FlatCompat({
	baseDirectory: __dirname,
	resolvePluginsRelativeTo: __dirname,
});

const migrated = compat.config(legacyConfig);

for (const ruleset of migrated) {
	ruleset.files = ["**/*.vue"];
}

export default migrated;
