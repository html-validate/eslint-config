module.exports = {
	env: {
		"cypress/globals": true,
	},

	plugins: ["cypress"],

	extends: ["plugin:cypress/recommended"],

	rules: {
		/* this rule triggers on basically every should(..) */
		"sonarjs/no-duplicate-string": "off",
	},
};
