import defaultConfig from "./packages/eslint-config/index.mjs";
import typescriptConfig from "./packages/eslint-config-typescript/index.mjs";
import jestConfig from "./packages/eslint-config-jest/index.mjs";

export default [
	{
		name: "Ignored files",
		ignores: [
			"**/coverage/**",
			"**/dist/**",
			"**/node_modules/**",
			"**/public/assets/**",
			"**/temp/**",
		],
	},
	...defaultConfig,
	...typescriptConfig,
	...jestConfig,
	{
		files: ["packages/eslint-config/src/cli.ts"],
		rules: {
			"no-console": "off",
		},
	},
];
