# @html-validate/eslint-config-monorepo changelog

## 5.28.0 (2025-03-10)

### Features

- **@html-validate/eslint-config-angularjs, @html-validate/eslint-config-cypress, @html-validate/eslint-config-jest, @html-validate/eslint-config-protractor, @html-validate/eslint-config-typescript-typeinfo, @html-validate/eslint-config-typescript, @html-validate/eslint-config-vue, @html-validate/eslint-config:** support ESLint flat configuration with `--flat-config` e54eaa8

### Bug Fixes

- **@html-validate/eslint-config-jest:** disable `sonarjs/no-nested-functions` in jest testcases 85e35d8
- **@html-validate/eslint-config:** disable `sonarjs/cognitive-complexity` in favor of `complexity` 01f1cd8
- **@html-validate/eslint-config:** disable `sonarjs/function-return-type` 95935fa
- **@html-validate/eslint-config:** disable `sonarjs/no-unused-vars` in favor of `@typescript-eslint/no-unused-vars` d1534de
- **@html-validate/eslint-config:** disable `sonarjs/todo-tag` 2395346

## 5.27.0 (2025-03-08)

### Features

- **deps:** update dependency eslint-plugin-vue to v10 5601637

### Bug Fixes

- **deps:** update dependency eslint-config-prettier to v10.1.0 5b78320
- **deps:** update dependency eslint-config-prettier to v10.1.1 c9937bb
- **deps:** update dependency eslint-plugin-n to v17.16.1 ec8b11a
- **deps:** update dependency eslint-plugin-n to v17.16.2 da2a994
- **deps:** update dependency eslint-plugin-vue to v9.33.0 a7f2c2b
- **deps:** update typescript-eslint monorepo to v8.26.0 c882f71

## 5.26.6 (2025-03-02)

### Bug Fixes

- **deps:** update dependency eslint-config-prettier to v10.0.2 053574c
- **deps:** update typescript-eslint monorepo to v8.25.0 c49d94e

## 5.26.5 (2025-02-23)

### Bug Fixes

- **deps:** update dependency eslint-import-resolver-typescript to v3.8.1 ae18043
- **deps:** update dependency eslint-import-resolver-typescript to v3.8.2 a54378b
- **deps:** update dependency eslint-import-resolver-typescript to v3.8.3 217839d
- **deps:** update typescript-eslint monorepo to v8.24.1 9883900

## 5.26.4 (2025-02-17)

### Bug Fixes

- **deps:** fix eslint peerDependency 0719902

## 5.26.3 (2025-02-16)

### Bug Fixes

- **deps:** update dependency eslint-import-resolver-typescript to v3.8.0 5922e93
- **deps:** update dependency eslint-plugin-sonarjs to v3.0.2 0fdcf50
- **deps:** update typescript-eslint monorepo to v8.24.0 b7a32a9

## 5.26.2 (2025-02-09)

### Bug Fixes

- **deps:** update typescript-eslint monorepo to v8.23.0 b8eaf38

## 5.26.1 (2025-02-02)

### Bug Fixes

- **deps:** update typescript-eslint monorepo to v8.22.0 4969eef

## 5.26.0 (2025-01-26)

### Features

- **deps:** update dependency eslint-config-prettier to v10 9c57ff4

### Bug Fixes

- **deps:** update dependency eslint-plugin-prettier to v5.2.3 8949d9a
- **deps:** update typescript-eslint monorepo to v8.21.0 6940a46

## 5.25.5 (2025-01-19)

### Bug Fixes

- **deps:** update dependency eslint-plugin-jest to v28.11.0 5b1f85c
- **deps:** update dependency eslint-plugin-prettier to v5.2.2 b0c66df
- **deps:** update typescript-eslint monorepo to v8.20.0 76ccff8

## 5.25.4 (2025-01-12)

### Bug Fixes

- **deps:** update dependency @rushstack/eslint-patch to v1.10.5 5b6c429
- **deps:** update typescript-eslint monorepo to v8.19.1 7258b52

## 5.25.3 (2025-01-05)

### Bug Fixes

- **deps:** update typescript-eslint monorepo to v8.19.0 af0ad20

## 5.25.2 (2024-12-29)

### Bug Fixes

- **deps:** update typescript-eslint monorepo to v8.18.2 e72417a

## 5.25.1 (2024-12-22)

### Bug Fixes

- **deps:** update dependency eslint-plugin-jest to v28.10.0 d2a4de6
- **deps:** update dependency eslint-plugin-n to v17.15.1 2e7147e
- **deps:** update typescript-eslint monorepo to v8.18.1 065689e

## 5.25.0 (2024-12-15)

### Features

- **deps:** update dependency eslint-plugin-sonarjs to v3 3e02aad

### Bug Fixes

- **deps:** update dependency eslint-plugin-n to v17.15.0 02609af
- **deps:** update typescript-eslint monorepo to v8.17.0 c1d9d87
- **deps:** update typescript-eslint monorepo to v8.18.0 f0abebb

## 5.24.1 (2024-12-08)

### Bug Fixes

- **deps:** update dependency eslint-import-resolver-typescript to v3.7.0 0b4d3a8
- **deps:** update dependency eslint-plugin-vue to v9.32.0 b027570

## 5.24.0 (2024-12-01)

### Features

- **deps:** update typescript-eslint monorepo to v8 6954afe

### Bug Fixes

- **@html-validate/eslint-config:** disable redundant sonarjs/unused-import 04bbf78

## 5.23.4 (2024-11-24)

### Bug Fixes

- **deps:** update dependency eslint-plugin-n to v17.14.0 408fca5
- **deps:** update dependency eslint-plugin-tsdoc to v0.4.0 72c7b38
- **deps:** update typescript-eslint monorepo to v8.14.0 0793eb8
- **deps:** update typescript-eslint monorepo to v8.15.0 33f0755

## 5.23.3 (2024-11-17)

### Bug Fixes

- **deps:** update dependency eslint-plugin-n to v17.13.2 c2d0f81
- **deps:** update dependency eslint-plugin-vue to v9.31.0 8a93872

## 5.23.2 (2024-11-10)

### Bug Fixes

- **deps:** update dependency eslint-plugin-jest to v28.9.0 8f6ba08
- **deps:** update dependency eslint-plugin-n to v17.13.0 93ff80f
- **deps:** update dependency eslint-plugin-n to v17.13.1 3356e3b
- **deps:** update typescript-eslint monorepo to v8.12.2 64f99d4
- **deps:** update typescript-eslint monorepo to v8.13.0 8837704

## 5.23.1 (2024-11-03)

### Bug Fixes

- **deps:** update dependency eslint-plugin-n to v17.12.0 4bdbc10
- **deps:** update dependency eslint-plugin-vue to v9.30.0 9bc0fd4

## 5.23.0 (2024-10-27)

### Features

- **deps:** require nodejs 20 aa74eac

### Bug Fixes

- **deps:** update dependency eslint-plugin-cypress to v3.6.0 0e7ac06
- **deps:** update dependency eslint-plugin-import to v2.31.0 e8ae2ea
- **deps:** update dependency eslint-plugin-n to v17.11.1 0793f5e
- **deps:** update dependency eslint-plugin-sonarjs to v2.0.3 1cdb7b7
- **deps:** update dependency eslint-plugin-sonarjs to v2.0.4 b6a53f2
- **deps:** update dependency eslint-plugin-vue to v9.29.1 88e1914
- **deps:** update typescript-eslint monorepo to v8.11.0 9fd36e7

## 5.22.7 (2024-09-29)

### Bug Fixes

- **deps:** update typescript-eslint monorepo to v8.6.0 8f95635
- **deps:** update typescript-eslint monorepo to v8.6.0 e2cbe37

## 5.22.6 (2024-09-22)

### Bug Fixes

- **deps:** update dependency eslint to v8.57.1 26e45c4
- **deps:** update dependency eslint-plugin-jasmine to v4.2.2 90b191d
- **deps:** update dependency eslint-plugin-n to v17.10.3 264ea4e

## 5.22.5 (2024-09-15)

### Bug Fixes

- **deps:** update dependency eslint-plugin-import to v2.30.0 5b5342a
- **deps:** update typescript-eslint monorepo to v8.5.0 173baee

## 5.22.4 (2024-09-08)

### Bug Fixes

- **deps:** update dependency eslint-plugin-jest to v28.8.2 2e1c8df
- **deps:** update dependency eslint-plugin-jest to v28.8.3 cc14bdc
- **deps:** update dependency eslint-plugin-vue to v9.28.0 c2b17f0
- **deps:** update typescript-eslint monorepo to v8.4.0 86a9d5d

## 5.22.3 (2024-09-01)

### Bug Fixes

- **@html-validate/eslint-config:** disable redundant sonarjs/sonar-no-control-regex be570ac
- **@html-validate/eslint-config:** disable redundant sonarjs/sonar-prefer-regexp-exec 81c6193
- **@html-validate/eslint-config:** disable sonarjs/no-empty-test-file which just yields false positives ffae462

## 5.22.2 (2024-09-01)

### Bug Fixes

- **@html-validate/eslint-config:** disable redundant sonarjs/deprecation bc1366d

## 5.22.1 (2024-08-31)

### Bug Fixes

- **@html-validate/eslint-config:** disable redundant sonarjs/prefer-nullish-coalescing 73fe6de
- **deps:** update dependency eslint-plugin-jest to v28.8.1 c9ab5f7
- **deps:** update dependency eslint-plugin-sonarjs to v2.0.2 8ae3979

## 5.22.0 (2024-08-29)

### Features

- **deps:** update dependency eslint-plugin-sonarjs to v2 806d174
- **deps:** update typescript-eslint monorepo to v8 798631a

### Bug Fixes

- **deps:** update dependency eslint-import-resolver-typescript to v3.6.3 0ed98ea

## 5.21.9 (2024-08-25)

### Bug Fixes

- **deps:** update dependency eslint-plugin-jasmine to v4.2.1 d3d4d81

## 5.21.8 (2024-08-18)

### Bug Fixes

- **deps:** update dependency eslint-plugin-cypress to v3.5.0 f3f509e

## 5.21.7 (2024-08-11)

### Bug Fixes

- **deps:** update dependency eslint-plugin-jest to v28.7.0 1ad06e3
- **deps:** update dependency eslint-plugin-jest to v28.8.0 87651c6
- **deps:** update dependency eslint-plugin-n to v17.10.2 0f8b367

## 5.21.6 (2024-08-04)

### Bug Fixes

- **deps:** update typescript-eslint monorepo to v7.18.0 cf6200c

## 5.21.5 (2024-07-28)

### Bug Fixes

- **deps:** update dependency @rushstack/eslint-patch to v1.10.4 d2342da
- **deps:** update dependency eslint-plugin-cypress to v3.4.0 0542e1e
- **deps:** update dependency eslint-plugin-n to v17.10.0 bb142cd
- **deps:** update dependency eslint-plugin-n to v17.10.1 5a46c43
- **deps:** update dependency eslint-plugin-sonarjs to v1.0.4 85e0c14
- **deps:** update typescript-eslint monorepo to v7.17.0 02be0df

## 5.21.4 (2024-07-21)

### Bug Fixes

- **deps:** update dependency eslint-plugin-prettier to v5.2.1 e18d3fd
- **deps:** update typescript-eslint monorepo to v7.16.1 69adf1f

## 5.21.3 (2024-07-14)

### Bug Fixes

- **deps:** update typescript-eslint monorepo to v7.16.0 ec43186

## 5.21.2 (2024-07-07)

### Bug Fixes

- **@html-validate/eslint-config-typescript-typeinfo:** fix `@typescript-eslint/consistent-type-imports` with inline type 44e958a
- **deps:** update dependency eslint-plugin-vue to v9.27.0 34f8e1a
- **deps:** update typescript-eslint monorepo to v7.15.0 92c80ff

## 5.21.1 (2024-06-30)

### Bug Fixes

- **deps:** update typescript-eslint monorepo to v7.14.1 07e70a1

## 5.21.0 (2024-06-22)

### Features

- **deps:** update dependency eslint-plugin-sonarjs to v1 83e1cbf

### Bug Fixes

- **deps:** update typescript-eslint monorepo to v7.13.1 cb65b69

## 5.20.5 (2024-06-16)

### Bug Fixes

- **deps:** update dependency eslint-plugin-jasmine to v4.2.0 479a56d
- **deps:** update dependency eslint-plugin-n to v17.9.0 bd5c02b
- **deps:** update dependency eslint-plugin-security to v3.0.1 abb72a8
- **deps:** update typescript-eslint monorepo to v7.13.0 1fcb4da

## 5.20.4 (2024-06-09)

### Bug Fixes

- **deps:** update dependency eslint-plugin-jest to v28.6.0 9ae2460
- **deps:** update dependency eslint-plugin-n to v17.8.0 4797bf2
- **deps:** update dependency eslint-plugin-n to v17.8.1 f772efa
- **deps:** update typescript-eslint monorepo to v7.12.0 997912f

## 5.20.3 (2024-06-02)

### Bug Fixes

- **deps:** update dependency eslint-plugin-cypress to v3.3.0 2262ea7
- **deps:** update dependency eslint-plugin-tsdoc to v0.3.0 2612348
- **deps:** update typescript-eslint monorepo to v7.10.0 c39e91f
- **deps:** update typescript-eslint monorepo to v7.11.0 486a8ad

## 5.20.2 (2024-05-19)

### Bug Fixes

- **deps:** update dependency @rushstack/eslint-patch to v1.10.3 88f08d2
- **deps:** update dependency eslint-plugin-n to v17.7.0 2197ede
- **deps:** update typescript-eslint monorepo to v7.9.0 7636ed5

## 5.20.1 (2024-05-12)

### Bug Fixes

- **deps:** update dependency eslint-plugin-cypress to v3.2.0 975a18b
- **deps:** update dependency eslint-plugin-n to v17.5.0 3aeeeca
- **deps:** update dependency eslint-plugin-n to v17.5.1 415360c
- **deps:** update dependency eslint-plugin-n to v17.6.0 b9d30b0
- **deps:** update dependency eslint-plugin-vue to v9.26.0 6398d3a

## 5.20.0 (2024-05-05)

### Features

- **deps:** update dependency eslint-plugin-cypress to v3 4c56ebf

### Bug Fixes

- **deps:** update dependency eslint-plugin-jest to v28.3.0 9e56f69
- **deps:** update dependency eslint-plugin-jest to v28.4.0 3d6fe76
- **deps:** update dependency eslint-plugin-jest to v28.5.0 c49569d
- **deps:** update dependency eslint-plugin-n to v17.3.0 2a4e856
- **deps:** update dependency eslint-plugin-n to v17.3.1 429d2cc
- **deps:** update dependency eslint-plugin-n to v17.4.0 95f2ea2
- **deps:** update typescript-eslint monorepo to v7.7.1 e986a90
- **deps:** update typescript-eslint monorepo to v7.8.0 5ecf9f1

## 5.19.0 (2024-04-21)

### Features

- **deps:** update dependency eslint-plugin-security to v3 27610d1

### Bug Fixes

- **deps:** update dependency eslint-plugin-cypress to v2.15.2 83cf6fe
- **deps:** update dependency eslint-plugin-n to v17.2.1 c5a3288
- **deps:** update dependency eslint-plugin-vue to v9.25.0 f488d8f
- **deps:** update typescript-eslint monorepo to v7.7.0 280988e

## 5.18.0 (2024-04-14)

### Features

- **deps:** update dependency eslint-plugin-n to v17 6b8a278

### Bug Fixes

- **deps:** update dependency @rushstack/eslint-patch to v1.10.2 2caef83
- **deps:** update dependency eslint-plugin-jest to v28.2.0 d6265f5
- **deps:** update dependency eslint-plugin-n to v17.2.0 6b89047
- **deps:** update dependency eslint-plugin-vue to v9.24.1 582d767
- **deps:** update typescript-eslint monorepo to v7.6.0 70a7f8a

## 5.17.0 (2024-04-07)

### Features

- **deps:** update dependency eslint-plugin-jest to v28 d435685

## 5.16.2 (2024-04-06)

### Bug Fixes

- **deps:** update dependency eslint-plugin-sonarjs to v0.25.1 03400b7
- **deps:** update typescript-eslint monorepo to v7.5.0 9fbd858

## 5.16.1 (2024-03-31)

### Bug Fixes

- **deps:** update dependency @rushstack/eslint-patch to v1.10.0 a98c958
- **deps:** update dependency @rushstack/eslint-patch to v1.10.1 f758070
- **deps:** update dependency @rushstack/eslint-patch to v1.9.0 c4c82d0
- **deps:** update dependency eslint-plugin-sonarjs to v0.25.0 0cc667d
- **deps:** update dependency eslint-plugin-vue to v9.24.0 551a3fe
- **deps:** update typescript-eslint monorepo to v7.4.0 03b85d1

## 5.16.0 (2024-03-21)

### Features

- **deps:** update dependency eslint-plugin-n to v16 c05b8ea

### Bug Fixes

- **deps:** replace dependency eslint-plugin-node with eslint-plugin-n 14.0.0 f8fc544
- **deps:** update dependency @rushstack/eslint-patch to v1.8.0 ab67b24
- **deps:** update typescript-eslint monorepo to v7.3.1 a371681

## 5.15.1 (2024-03-20)

### Bug Fixes

- **deps:** update dependency eslint-plugin-vue to v9.23.0 c33646b
- **deps:** update typescript-eslint monorepo to v7.2.0 bcb9cdb

## 5.15.0 (2024-03-10)

### Features

- **@html-validate/eslint-config:** bump ecmaVersion to 2023 9d9f192

### Bug Fixes

- **deps:** update typescript-eslint monorepo to v7.1.0 8098a6b
- **deps:** update typescript-eslint monorepo to v7.1.1 1670652

## 5.14.0 (2024-2-25)

### Features

- **deps:** update typescript-eslint monorepo to v7 4882aae

### Bug Fixes

- **deps:** update dependency eslint to v8.57.0 ca43acd
- **deps:** update dependency eslint-plugin-vue to v9.22.0 b027529

## 5.13.2 (2024-2-18)

### Bug Fixes

- **deps:** update dependency eslint-plugin-jest to v27.8.0 8672883
- **deps:** update dependency eslint-plugin-jest to v27.9.0 3e92378
- **deps:** update dependency eslint-plugin-security to v2.1.1 26246df
- **deps:** update dependency eslint-plugin-sonarjs to v0.24.0 dcee931

## 5.13.1 (2024-2-11)

### Bug Fixes

- **deps:** update typescript-eslint monorepo to v6.21.0 c244585

## 5.13.0 (2024-2-2)

### Features

- **deps:** require node 18 3859eea

### Bug Fixes

- **deps:** update dependency eslint-plugin-vue to v9.21.0 1865c1c
- **deps:** update dependency eslint-plugin-vue to v9.21.1 bf14d40
- **deps:** update typescript-eslint monorepo to v6.20.0 29f8b5d

## 5.12.9 (2024-1-27)

### Bug Fixes

- **deps:** update dependency @rushstack/eslint-patch to v1.7.1 81c5e57
- **deps:** update dependency @rushstack/eslint-patch to v1.7.2 7771976
- **deps:** update typescript-eslint monorepo to v6.19.1 4085365

## 5.12.8 (2024-1-21)

### Bug Fixes

- **deps:** update dependency @rushstack/eslint-patch to v1.7.0 568fbd5
- **deps:** update typescript-eslint monorepo to v6.19.0 840d89f

## 5.12.7 (2024-1-14)

### Bug Fixes

- **deps:** update dependency eslint-plugin-jest to v27.6.2 28f7a86
- **deps:** update dependency eslint-plugin-jest to v27.6.3 d1218a7
- **deps:** update dependency eslint-plugin-prettier to v5.1.3 605a561
- **deps:** update dependency eslint-plugin-vue to v9.20.0 ae0d98f
- **deps:** update dependency eslint-plugin-vue to v9.20.1 126e655
- **deps:** update typescript-eslint monorepo to v6.18.0 2d7e87e
- **deps:** update typescript-eslint monorepo to v6.18.1 899b24a

## 5.12.6 (2024-1-7)

### Bug Fixes

- **deps:** update dependency eslint-plugin-jest to v27.6.1 dc63ed1
- **deps:** update typescript-eslint monorepo to v6.17.0 f644283

## 5.12.5 (2023-12-28)

### Bug Fixes

- **deps:** update dependency eslint-plugin-prettier to v5.1.2 ce0e53f
- **deps:** update dependency eslint-plugin-security to v2 dcff713
- **deps:** update typescript-eslint monorepo to v6.16.0 dd6ee43

## 5.12.4 (2023-12-24)

### Bug Fixes

- **deps:** update dependency eslint-plugin-prettier to v5.1.0 114c1be
- **deps:** update dependency eslint-plugin-prettier to v5.1.1 83865a4
- **deps:** update typescript-eslint monorepo to v6.15.0 a8cb422

## 5.12.3 (2023-12-17)

### Bug Fixes

- **deps:** update dependency @rushstack/eslint-patch to v1.6.1 9cf3197
- **deps:** update dependency eslint to v8.56.0 9e107e8
- **deps:** update dependency eslint-plugin-import to v2.29.1 0866840
- **deps:** update typescript-eslint monorepo to v6.14.0 fe67485

## 5.12.2 (2023-12-10)

### Bug Fixes

- **deps:** update dependency eslint-config-prettier to v9.1.0 acaa30b
- **deps:** update typescript-eslint monorepo to v6.13.2 7428733

## 5.12.1 (2023-12-3)

### Bug Fixes

- **deps:** update dependency eslint to v8.55.0 39115bc
- **deps:** update dependency eslint-plugin-vue to v9.19.1 dea2bee
- **deps:** update dependency eslint-plugin-vue to v9.19.2 d683305
- **deps:** update typescript-eslint monorepo to v6.13.0 58582b9
- **deps:** update typescript-eslint monorepo to v6.13.1 646d743

## 5.12.0 (2023-11-26)

### Features

- **@html-validate/eslint-config-angularjs, @html-validate/eslint-config-cypress, @html-validate/eslint-config-jest, @html-validate/eslint-config-protractor, @html-validate/eslint-config-typescript-typeinfo, @html-validate/eslint-config-typescript, @html-validate/eslint-config-vue, @html-validate/eslint-config:** require nodejs 16 or later efd9676

## 5.11.12 (2023-11-23)

### Bug Fixes

- **deps:** update dependency @rushstack/eslint-patch to v1.6.0 58ad7ba
- **deps:** update dependency eslint-plugin-prettier to v5 661c627
- **deps:** update typescript-eslint monorepo to v6.12.0 479bf73

## 5.11.11 (2023-11-19)

### Bug Fixes

- **deps:** update dependency eslint to v8.54.0 f8dc6a2
- **deps:** update typescript-eslint monorepo to v6.11.0 5b04610

## 5.11.10 (2023-11-12)

### Bug Fixes

- **deps:** update dependency eslint-formatter-gitlab to v5.1.0 0ebdc88
- **deps:** update typescript-eslint monorepo to v6.10.0 0d42f97

## 5.11.9 (2023-11-05)

### Bug Fixes

- **deps:** update dependency eslint to v8.53.0 f96d58a
- **deps:** update dependency eslint-plugin-sonarjs to v0.22.0 c869c27
- **deps:** update dependency eslint-plugin-sonarjs to v0.23.0 97e126b
- **deps:** update typescript-eslint monorepo to v6.9.1 b118638

## 5.11.8 (2023-10-29)

### Bug Fixes

- **deps:** update dependency eslint-plugin-import to v2.29.0 e9cd19f
- **deps:** update dependency eslint-plugin-jest to v27.5.0 a3f83f1
- **deps:** update dependency eslint-plugin-jest to v27.6.0 8eafc3f
- **deps:** update dependency eslint-plugin-vue to v9.18.0 63e7832
- **deps:** update dependency eslint-plugin-vue to v9.18.1 cf082e9
- **deps:** update typescript-eslint monorepo to v6.9.0 1c52de5

## 5.11.7 (2023-10-22)

### Bug Fixes

- **deps:** update dependency eslint to v8.52.0 2fab856
- **deps:** update dependency eslint-plugin-jest to v27.4.3 e0bda60
- **deps:** update typescript-eslint monorepo to v6.8.0 437df45

## 5.11.6 (2023-10-15)

### Dependency upgrades

- **deps:** update typescript-eslint monorepo to v6.7.5 47153df

## 5.11.5 (2023-10-08)

### Dependency upgrades

- **deps:** update dependency @rushstack/eslint-patch to v1.5.1 c5963a5
- **deps:** update dependency eslint to v8.51.0 11fffb8
- **deps:** update typescript-eslint monorepo to v6.7.4 9ddeef1

## 5.11.4 (2023-10-01)

### Dependency upgrades

- **deps:** update dependency @rushstack/eslint-patch to v1.5.0 a69b4e3
- **deps:** update dependency eslint-plugin-jest to v27.4.2 77f95f8
- **deps:** update typescript-eslint monorepo to v6.7.3 3d2c4b8

## 5.11.3 (2023-09-24)

### Dependency upgrades

- **deps:** update dependency eslint to v8.50.0 9ab9b92
- **deps:** update dependency eslint-import-resolver-typescript to v3.6.1 6406e73
- **deps:** update dependency eslint-plugin-cypress to v2.15.1 bbc14d9
- **deps:** update typescript-eslint monorepo to v6.7.2 af5ec4f

## 5.11.2 (2023-09-17)

### Dependency upgrades

- **deps:** update dependency @rushstack/eslint-patch to v1.4.0 5c4c898
- **deps:** update dependency eslint-plugin-array-func to v4 6948089
- **deps:** update dependency eslint-plugin-jest to v27.4.0 2fac2d3
- **deps:** update typescript-eslint monorepo to v6.7.0 0cc9298

## 5.11.1 (2023-09-10)

### Dependency upgrades

- **deps:** update dependency eslint to v8.49.0 3d7b61e

## 5.11.0 (2023-09-09)

### Features

- **@html-validate/eslint-config:** enable object-shorthand bc5822f

### Dependency upgrades

- **deps:** update typescript-eslint monorepo to v6.5.0 0113cca
- **deps:** update typescript-eslint monorepo to v6.6.0 76bf92c

## 5.10.9 (2023-08-27)

### Dependency upgrades

- **deps:** update dependency eslint to v8.48.0 20053c1
- **deps:** update dependency eslint-plugin-sonarjs to v0.21.0 4731063
- **deps:** update typescript-eslint monorepo to v6.4.1 e6fa9fe

## 5.10.8 (2023-08-20)

### Dependency upgrades

- **deps:** update dependency eslint-plugin-import to v2.28.1 b223583
- **deps:** update typescript-eslint monorepo to v6.4.0 5077bb7

## 5.10.7 (2023-08-13)

### Dependency upgrades

- **deps:** update dependency @rushstack/eslint-patch to v1.3.3 b6cb63d
- **deps:** update dependency eslint to v8.47.0 9bd3e9e
- **deps:** update dependency eslint-config-prettier to v9 3cc05d5
- **deps:** update dependency eslint-import-resolver-node to v0.3.8 75b2ded
- **deps:** update dependency eslint-import-resolver-node to v0.3.9 16a1048
- **deps:** update dependency eslint-import-resolver-typescript to v3.6.0 4793742
- **deps:** update dependency eslint-plugin-cypress to v2.13.4 33a5960
- **deps:** update dependency eslint-plugin-cypress to v2.14.0 f57a589
- **deps:** update dependency eslint-plugin-vue to v9.17.0 9e31ed4
- **deps:** update typescript-eslint monorepo to v6.3.0 7340372

## 5.10.6 (2023-08-06)

### Dependency upgrades

- **deps:** update dependency eslint-config-prettier to v8.10.0 905ca1d
- **deps:** update dependency eslint-plugin-sonarjs to v0.20.0 5835d7d
- **deps:** update dependency eslint-plugin-vue to v9.16.0 5596cde
- **deps:** update dependency eslint-plugin-vue to v9.16.1 0e98d01
- **deps:** update typescript-eslint monorepo to v6.2.1 2a0e103

## 5.10.5 (2023-07-30)

### Dependency upgrades

- **deps:** update dependency eslint to v8.46.0 3ba71b3
- **deps:** update dependency eslint-plugin-import to v2.28.0 d46f597

## 5.10.4 (2023-07-28)

### Bug Fixes

- **typescript:** allow overloading only with different parameter names 881d286
- **typescript:** disable `@typescript-eslint/class-literal-property-style` c8bb3a1

### Dependency upgrades

- **deps:** update dependency eslint-config-prettier to v8.9.0 7abc40d
- **deps:** update dependency eslint-formatter-gitlab to v5 33fda16
- **deps:** update typescript-eslint monorepo to v6.2.0 84085d3

## 5.10.3 (2023-07-22)

### Bug Fixes

- **typescript:** disable `@typescript-eslint/no-redundant-type-constituents` 69ba2da

### Dependency upgrades

- **deps:** update typescript-eslint monorepo to v6.1.0 512fed1

## 5.10.2 (2023-07-15)

### Bug Fixes

- **typescript:** allow this as `void` in functions c2f0e4a

### Dependency upgrades

- **deps:** update dependency eslint to v8.45.0 d0da1d1

## 5.10.1 (2023-07-15)

### Bug Fixes

- **typescript:** enable strict rules again 26d31fc

## 5.10.0 (2023-07-15)

### Features

- **@html-validate/eslint-config-typescript-typeinfo, @html-validate/eslint-config-typescript:** enable `@typescript-eslint/array-type` 1835519
- **deps:** update typescript-eslint monorepo to v6 f6318ec

### Dependency upgrades

- **deps:** update dependency eslint-plugin-jest to v27.2.3 7342286
- **deps:** update typescript-eslint monorepo to v5.62.0 55f03e8

## 5.9.13 (2023-07-09)

### Dependency upgrades

- **deps:** update typescript-eslint monorepo to v5.61.0 d3ae2dd

## 5.9.12 (2023-07-02)

### Dependency upgrades

- **deps:** update dependency eslint to v8.44.0 facfd20
- **deps:** update dependency eslint-plugin-vue to v9.15.1 0cbfb85
- **deps:** update typescript-eslint monorepo to v5.60.1 66b7dd1

## 5.9.11 (2023-06-25)

### Dependency upgrades

- **deps:** update dependency eslint-plugin-jest to v27.2.2 30ed4b8
- **deps:** update dependency eslint-plugin-vue to v9.15.0 754dcdb
- **deps:** update typescript-eslint monorepo to v5.60.0 82af8a9

## 5.9.10 (2023-06-18)

### Dependency upgrades

- **deps:** update dependency @rushstack/eslint-patch to v1.3.2 d5ce91f
- **deps:** update dependency eslint to v8.43.0 503a38a
- **deps:** update typescript-eslint monorepo to v5.59.11 a6b21f4

## 5.9.9 (2023-06-11)

### Dependency upgrades

- **deps:** update dependency @rushstack/eslint-patch to v1.3.1 0ae2f64
- **deps:** update typescript-eslint monorepo to v5.59.9 ddd86b0

## 5.9.8 (2023-06-04)

### Dependency upgrades

- **deps:** update dependency eslint to v8.42.0 1bfe00e
- **deps:** update typescript-eslint monorepo to v5.59.8 5a9ada8

## 5.9.7 (2023-05-28)

### Dependency upgrades

- **deps:** update dependency @rushstack/eslint-patch to v1.3.0 fc341e4
- **deps:** update dependency eslint-plugin-vue to v9.14.0 a25c3f2
- **deps:** update dependency eslint-plugin-vue to v9.14.1 a2c530e
- **deps:** update typescript-eslint monorepo to v5.59.7 c232ea5

## 5.9.6 (2023-05-21)

### Dependency upgrades

- **deps:** update dependency eslint to v8.41.0 0feda40
- **deps:** update dependency eslint-plugin-vue to v9.13.0 e28969a
- **deps:** update typescript-eslint monorepo to v5.59.6 2c75412

## 5.9.5 (2023-05-14)

### Dependency upgrades

- **deps:** update dependency eslint-plugin-vue to v9.11.1 c9362ea
- **deps:** update dependency eslint-plugin-vue to v9.12.0 e86eeea
- **deps:** update typescript-eslint monorepo to v5.59.5 0d3bddd

## 5.9.4 (2023-05-07)

### Dependency upgrades

- **deps:** update dependency eslint to v8.40.0 4817c0f
- **deps:** update typescript-eslint monorepo to v5.59.2 5a7b206

## 5.9.3 (2023-04-30)

### Dependency upgrades

- **deps:** update dependency eslint-plugin-cypress to v2.13.3 0c2e5f0
- **deps:** update typescript-eslint monorepo to v5.59.1 9fe999a

## 5.9.2 (2023-04-23)

### Dependency upgrades

- **deps:** update dependency eslint to v8.39.0 4ee486f
- **deps:** update typescript-eslint monorepo to v5.59.0 6ae9dca

## 5.9.1 (2023-04-16)

### Dependency upgrades

- **deps:** update dependency eslint-plugin-vue to v9.11.0 7850103

## 5.9.0 (2023-04-15)

### Features

- **@html-validate/eslint-config:** add comment at top of config 049f01b
- **@html-validate/eslint-config:** lint _.mjs and _.cjs by default 86a3a3f
- **@html-validate/eslint-config:** use `--cache` when running eslint 537706e

### Bug Fixes

- **@html-validate/eslint-config:** ensure override block is always present 1fbe684

## 5.8.0 (2023-04-15)

### Features

- **@html-validate/eslint-config-jest:** allow `!` non-null assertion in jest tests a68e731
- **@html-validate/eslint-config:** disable some rules for bin and dts files which depends on project being built a4b1f96
- **@html-validate/eslint-config:** prefer `.eslintrc.cjs` over `.eslintrc.js` ccb906c

### Bug Fixes

- **@html-validate/eslint-config:** ignore temp folder baef30c

### Dependency upgrades

- **deps:** update typescript-eslint monorepo to v5.58.0 7b285e8

## 5.7.3 (2023-04-09)

### Dependency upgrades

- **deps:** update dependency eslint to v8.38.0 a2385a2
- **deps:** update dependency eslint-import-resolver-typescript to v3.5.5 c90676d
- **deps:** update typescript-eslint monorepo to v5.57.1 c0cd8b1

## 5.7.2 (2023-04-02)

### Dependency upgrades

- **deps:** update dependency eslint to v8.37.0 8e139c2
- **deps:** update dependency eslint-import-resolver-typescript to v3.5.4 a70684a
- **deps:** update dependency eslint-plugin-cypress to v2.13.1 e7a563f
- **deps:** update dependency eslint-plugin-cypress to v2.13.2 d1da8f8
- **deps:** update typescript-eslint monorepo to v5.57.0 14fbc98

## 5.7.1 (2023-03-26)

### Bug Fixes

- **@html-validate/eslint-config:** dont require description for `eslint-enable` comments 61bc2ae

## 5.7.0 (2023-03-25)

### Features

- **@html-validate/eslint-config:** disable `security/detect-child-process` and `security/detect-object-injection` 5a4372f
- **@html-validate/eslint-config:** use eslint-plugin-comments 63c6ae1

## 5.6.0 (2023-03-25)

### Features

- **@html-validate/eslint-config:** include `eslint-import-resolver-{node,typescript}` f471e87

### Dependency upgrades

- **deps:** update dependency eslint-config-prettier to v8.8.0 88208ea
- **deps:** update dependency eslint-plugin-sonarjs to v0.19.0 101d5db
- **deps:** update dependency eslint-plugin-vue to v9.10.0 0f581f1
- **deps:** update typescript-eslint monorepo to v5.56.0 7c51b7f

## 5.5.28 (2023-03-19)

### Dependency upgrades

- **deps:** update typescript-eslint monorepo to v5.55.0 d84d644

## 5.5.27 (2023-03-12)

### Dependency upgrades

- **deps:** update dependency eslint to v8.36.0 64526ec
- **deps:** update dependency eslint-config-prettier to v8.7.0 f8666aa
- **deps:** update typescript-eslint monorepo to v5.54.1 696ae6d

## 5.5.26 (2023-03-05)

### Dependency upgrades

- **deps:** update dependency eslint to v8.35.0 1510b3e
- **deps:** update typescript-eslint monorepo to v5.54.0 bc938ae

## 5.5.25 (2023-02-26)

### Dependency upgrades

- **deps:** update typescript-eslint monorepo to v5.53.0 354117b

## 5.5.24 (2023-02-19)

### Dependency upgrades

- **deps:** update typescript-eslint monorepo to v5.52.0 e013ec2

## 5.5.23 (2023-02-12)

### Dependency upgrades

- **deps:** update dependency eslint to v8.34.0 9ba8ffc
- **deps:** update typescript-eslint monorepo to v5.51.0 6b4ddf5

## 5.5.22 (2023-02-05)

### Dependency upgrades

- **deps:** update dependency eslint to v8.33.0 5ee8c55
- **deps:** update dependency eslint-plugin-security to v1.7.1 cbf848b
- **deps:** update typescript-eslint monorepo to v5.50.0 38aca4f

## 5.5.21 (2023-01-29)

### Dependency upgrades

- **deps:** update dependency eslint-plugin-security to v1.7.0 3669abb
- **deps:** update typescript-eslint monorepo to v5.49.0 bd1a82a

## 5.5.20 (2023-01-22)

### Dependency upgrades

- **deps:** update dependency eslint to v8.32.0 5250f9d
- **deps:** update dependency eslint-plugin-import to v2.27.5 e8753be
- **deps:** update typescript-eslint monorepo to v5.48.2 a1a000a

## 5.5.19 (2023-01-15)

### Dependency upgrades

- **deps:** update dependency eslint-plugin-import to v2.27.4 99dcda6
- **deps:** update dependency eslint-plugin-security to v1.6.0 8678536
- **deps:** update dependency eslint-plugin-sonarjs to v0.18.0 72f2459
- **deps:** update dependency eslint-plugin-vue to v9.9.0 982b9af
- **deps:** update typescript-eslint monorepo to v5.48.1 101885a

## 5.5.18 (2023-01-08)

### Dependency upgrades

- **deps:** update dependency eslint to v8.31.0 4a76ef3
- **deps:** update dependency eslint-config-prettier to v8.6.0 6dabeaa
- **deps:** update dependency eslint-plugin-array-func to v3.1.8 1dbf366
- **deps:** update dependency eslint-plugin-jest to v27.2.0 4fe9579
- **deps:** update dependency eslint-plugin-jest to v27.2.1 41dca77
- **deps:** update typescript-eslint monorepo to v5.48.0 0a62f16

## 5.5.17 (2023-01-01)

### Dependency upgrades

- **deps:** update typescript-eslint monorepo to v5.47.1 6889915

## 5.5.16 (2022-12-25)

### Dependency upgrades

- **deps:** update typescript-eslint monorepo to v5.47.0 85e4d14

## 5.5.15 (2022-12-18)

### Dependency upgrades

- **deps:** update dependency eslint to v8.30.0 a5f2833
- **deps:** update dependency eslint-plugin-jest to v27.1.7 e175551
- **deps:** update typescript-eslint monorepo to v5.46.1 729c562

## 5.5.14 (2022-12-11)

### Dependency upgrades

- **deps:** update dependency eslint-plugin-sonarjs to v0.17.0 4ab433b
- **deps:** update typescript-eslint monorepo to v5.45.1 392eb2f
- **deps:** update typescript-eslint monorepo to v5.46.0 b990634

## 5.5.13 (2022-12-04)

### Dependency upgrades

- **deps:** update dependency eslint to v8.29.0 d3ef657
- **deps:** update typescript-eslint monorepo to v5.45.0 88ff8c8

## 5.5.12 (2022-11-27)

### Dependency upgrades

- **deps:** update dependency eslint-plugin-jest to v27.1.6 06670d6
- **deps:** update dependency eslint-plugin-vue to v9.8.0 7ecc44e
- **deps:** update typescript-eslint monorepo to v5.44.0 cdacc7b

## 5.5.11 (2022-11-20)

### Dependency upgrades

- **deps:** update dependency eslint to v8.28.0 3d3c76c
- **deps:** update typescript-eslint monorepo to v5.43.0 75dd0f0

## 5.5.10 (2022-11-13)

### Dependency upgrades

- **deps:** update dependency eslint to v8.27.0 f4c4767
- **deps:** update dependency eslint-formatter-gitlab to v4 0d6b300
- **deps:** update dependency eslint-plugin-jest to v27.1.5 2826768
- **deps:** update typescript-eslint monorepo to v5.42.1 ab8f7f0

## 5.5.9 (2022-11-06)

### Dependency upgrades

- **deps:** update dependency eslint-plugin-jest to v27.1.4 0758c31

## 5.5.8 (2022-11-01)

### Bug Fixes

- **@html-validate/eslint-config:** support es2020 5625aff

### Dependency upgrades

- **deps:** update dependency eslint-plugin-vue to v9.7.0 7abc2fe
- **deps:** update typescript-eslint monorepo to v5.42.0 42aa05f

## 5.5.7 (2022-10-30)

### Dependency upgrades

- **deps:** update typescript-eslint monorepo to v5.41.0 0183a63

## 5.5.6 (2022-10-23)

### Dependency upgrades

- **deps:** update dependency eslint to v8.26.0 1cc32e3
- **deps:** update dependency eslint-plugin-jest to v27.1.3 e49d484
- **deps:** update typescript-eslint monorepo to v5.40.1 7e7ad24

## 5.5.5 (2022-10-16)

### Dependency upgrades

- **deps:** update dependency eslint-plugin-jest to v27.1.2 34d5665
- **deps:** update dependency eslint-plugin-sonarjs to v0.16.0 88c7bf7
- **deps:** update typescript-eslint monorepo to v5.40.0 c367609

## 5.5.4 (2022-10-09)

### Dependency upgrades

- **deps:** update dependency eslint to v8.25.0 cd7faef
- **deps:** update dependency eslint-plugin-jest to v27.1.0 b667674
- **deps:** update dependency eslint-plugin-jest to v27.1.1 caad10a
- **deps:** update dependency eslint-plugin-vue to v9.6.0 84cbf9f
- **deps:** update typescript-eslint monorepo to v5.39.0 bb3c681

## 5.5.3 (2022-10-02)

### Dependency upgrades

- **deps:** update typescript-eslint monorepo to v5.38.1 4e46c7c

## 5.5.2 (2022-09-25)

### Dependency upgrades

- **deps:** update dependency eslint to v8.24.0 46d09fc
- **deps:** update typescript-eslint monorepo to v5.38.0 ecd85e3

## 5.5.1 (2022-09-18)

### Dependency upgrades

- **deps:** update dependency @rushstack/eslint-patch to v1.2.0 1e7e905
- **deps:** update dependency eslint to v8.23.1 1088f86
- **deps:** update dependency eslint-plugin-jest to v27.0.4 d8242ca
- **deps:** update dependency eslint-plugin-tsdoc to v0.2.17 38088c4
- **deps:** update dependency eslint-plugin-vue to v9.5.1 5226b8f
- **deps:** update typescript-eslint monorepo to v5.37.0 a764bc3

## 5.5.0 (2022-09-11)

### Features

- drop support for node 12 6f3bc46

### Dependency upgrades

- **deps:** update dependency eslint-plugin-jest to v27 5517ee7
- **deps:** update typescript-eslint monorepo to v5.36.2 e6d7ccb

## 5.4.23 (2022-09-04)

### Dependency upgrades

- **deps:** update dependency eslint-plugin-jest to v26.9.0 97e168a
- **deps:** update typescript-eslint monorepo to v5.36.1 1cd63bd

## 5.4.22 (2022-08-28)

### Dependency upgrades

- **deps:** update dependency eslint to v8.23.0 1ad4de5
- **deps:** update dependency eslint-plugin-jest to v26.8.7 4f77776
- **deps:** update dependency eslint-plugin-vue to v9.4.0 c116c63
- **deps:** update typescript-eslint monorepo to v5.34.0 763fcf6
- **deps:** update typescript-eslint monorepo to v5.35.1 058553c

## 5.4.21 (2022-08-21)

### Dependency upgrades

- **deps:** update dependency eslint to v8.22.0 f74b1e5
- **deps:** update dependency eslint-plugin-jest to v26.8.3 13e52c1
- **deps:** update typescript-eslint monorepo to v5.33.1 387c6de

## 5.4.20 (2022-08-14)

### Dependency upgrades

- **deps:** update dependency eslint-plugin-jest to v26.8.2 2492812
- **deps:** update dependency eslint-plugin-sonarjs to v0.15.0 55f7dff
- **deps:** update typescript-eslint monorepo to v5.33.0 f0aeeec

## 5.4.19 (2022-08-07)

### Dependency upgrades

- **deps:** update dependency eslint to v8.21.0 b587b82
- **deps:** update typescript-eslint monorepo to v5.32.0 1dcf1c1

## 5.4.18 (2022-07-31)

### Dependency upgrades

- **deps:** update dependency eslint-plugin-jest to v26.7.0 e9e8ac4
- **deps:** update dependency eslint-plugin-vue to v9.3.0 e7da067
- **deps:** update typescript-eslint monorepo to v5.31.0 733240e

## 5.4.17 (2022-07-24)

### Dependency upgrades

- **deps:** update dependency eslint to v8.20.0 b3c5dc2
- **deps:** update dependency eslint-plugin-sonarjs to v0.14.0 95aade3
- **deps:** update typescript-eslint monorepo to v5.30.7 85b2d26

## 5.4.16 (2022-07-17)

### Dependency upgrades

- **deps:** update dependency eslint-plugin-jest to v26.6.0 638a151
- **deps:** update typescript-eslint monorepo to v5.30.6 0203293

## 5.4.15 (2022-07-10)

### Dependency upgrades

- **deps:** update dependency eslint-plugin-vue to v9.2.0 8553dcc
- **deps:** update typescript-eslint monorepo to v5.30.4 260a584
- **deps:** update typescript-eslint monorepo to v5.30.5 de6fdf7

## 5.4.14 (2022-07-03)

### Dependency upgrades

- **deps:** update dependency @rushstack/eslint-patch to v1.1.4 09ea4f0
- **deps:** update dependency eslint to v8.19.0 4f53f72
- **deps:** update dependency eslint-plugin-prettier to v4.1.0 d326fc2
- **deps:** update dependency eslint-plugin-prettier to v4.2.1 6f31ded
- **deps:** update typescript-eslint monorepo to v5.30.0 a601eea
- **deps:** update typescript-eslint monorepo to v5.30.3 24cf1c2

## 5.4.13 (2022-06-26)

### Dependency upgrades

- **deps:** update typescript-eslint monorepo to v5.29.0 37cc64d

## 5.4.12 (2022-06-19)

### Dependency upgrades

- **deps:** update dependency eslint to v8.18.0 00fa05a

## 5.4.11 (2022-06-16)

### Bug Fixes

- **@html-validate/eslint-config:** support cypress 10 `*.cy.[jt]s` a189e62

### Dependency upgrades

- **deps:** update dependency eslint-plugin-jest to v26.5.3 fa21814
- **deps:** update dependency eslint-plugin-vue to v9.1.1 66f2433
- **deps:** update typescript-eslint monorepo to v5.27.1 3e2f951
- **deps:** update typescript-eslint monorepo to v5.28.0 9609a6c

### 5.4.10 (2022-06-05)

### Bug Fixes

- **@html-validate/eslint-config-typescript, @html-validate/eslint-config:** add `_` ignore pattern for `no-unused-vars` 619d6f6
- **@html-validate/eslint-config-typescript, @html-validate/eslint-config:** enable `ignoreRestSiblings` for `no-unused-vars` e35c171

### Dependency upgrades

- **deps:** update dependency eslint to v8.17.0 a2b8809
- **deps:** update dependency eslint-plugin-jest to v26.4.6 31ac6ed
- **deps:** update dependency eslint-plugin-vue to v9.1.0 1d3e2aa
- **deps:** update typescript-eslint monorepo to v5.27.0 aa22e0a

### 5.4.9 (2022-05-25)

### Dependency upgrades

- **deps:** update dependency eslint to v8.16.0 6bc22e3
- **deps:** update dependency eslint-plugin-vue to v9 0d61c4f
- **deps:** update typescript-eslint monorepo to v5.26.0 697b406

### 5.4.8 (2022-05-22)

### Dependency upgrades

- **deps:** update dependency eslint-plugin-jest to v26.2.2 f8d5325
- **deps:** update typescript-eslint monorepo to v5.25.0 9f6771a

### 5.4.7 (2022-05-15)

### Dependency upgrades

- **deps:** update typescript-eslint monorepo to v5.23.0 822ae50

### 5.4.6 (2022-05-08)

### Dependency upgrades

- **deps:** update dependency eslint to v8.15.0 11ea712

### 5.4.5 (2022-05-06)

### Dependency upgrades

- **deps:** update dependency @rushstack/eslint-patch to v1.1.2 68a908f
- **deps:** update dependency @rushstack/eslint-patch to v1.1.3 fa82fab
- **deps:** update dependency eslint to v8.13.0 49f8040
- **deps:** update dependency eslint to v8.14.0 060faff
- **deps:** update dependency eslint-plugin-import to v2.26.0 dbea9bb
- **deps:** update dependency eslint-plugin-jest to v26.1.4 a8eea3b
- **deps:** update dependency eslint-plugin-jest to v26.1.5 9065b1e
- **deps:** update dependency eslint-plugin-security to v1.5.0 7972341
- **deps:** update dependency eslint-plugin-sonarjs to v0.13.0 401fa51
- **deps:** update dependency eslint-plugin-tsdoc to v0.2.15 d61aa80
- **deps:** update dependency eslint-plugin-tsdoc to v0.2.16 6eb13ff
- **deps:** update dependency eslint-plugin-vue to v8.6.0 6d0a884
- **deps:** update dependency eslint-plugin-vue to v8.7.1 ebbc0b8
- **deps:** update typescript-eslint monorepo to v5.17.0 80fda8a
- **deps:** update typescript-eslint monorepo to v5.18.0 de6fdd1
- **deps:** update typescript-eslint monorepo to v5.19.0 356deef
- **deps:** update typescript-eslint monorepo to v5.20.0 3ef2692
- **deps:** update typescript-eslint monorepo to v5.21.0 afe3938
- **deps:** update typescript-eslint monorepo to v5.22.0 490f97c

### 5.4.5 (2022-05-06)

### Dependency upgrades

- **deps:** update dependency @rushstack/eslint-patch to v1.1.2 68a908f
- **deps:** update dependency @rushstack/eslint-patch to v1.1.3 fa82fab
- **deps:** update dependency eslint to v8.13.0 49f8040
- **deps:** update dependency eslint to v8.14.0 060faff
- **deps:** update dependency eslint-plugin-import to v2.26.0 dbea9bb
- **deps:** update dependency eslint-plugin-jest to v26.1.4 a8eea3b
- **deps:** update dependency eslint-plugin-jest to v26.1.5 9065b1e
- **deps:** update dependency eslint-plugin-security to v1.5.0 7972341
- **deps:** update dependency eslint-plugin-sonarjs to v0.13.0 401fa51
- **deps:** update dependency eslint-plugin-tsdoc to v0.2.15 d61aa80
- **deps:** update dependency eslint-plugin-tsdoc to v0.2.16 6eb13ff
- **deps:** update dependency eslint-plugin-vue to v8.6.0 6d0a884
- **deps:** update dependency eslint-plugin-vue to v8.7.1 ebbc0b8
- **deps:** update typescript-eslint monorepo to v5.17.0 80fda8a
- **deps:** update typescript-eslint monorepo to v5.18.0 de6fdd1
- **deps:** update typescript-eslint monorepo to v5.19.0 356deef
- **deps:** update typescript-eslint monorepo to v5.20.0 3ef2692
- **deps:** update typescript-eslint monorepo to v5.21.0 afe3938
- **deps:** update typescript-eslint monorepo to v5.22.0 490f97c

### 5.4.5 (2022-05-01)

### Dependency upgrades

- **deps:** update dependency @rushstack/eslint-patch to v1.1.2 68a908f
- **deps:** update dependency @rushstack/eslint-patch to v1.1.3 fa82fab
- **deps:** update dependency eslint to v8.13.0 49f8040
- **deps:** update dependency eslint to v8.14.0 060faff
- **deps:** update dependency eslint-plugin-import to v2.26.0 dbea9bb
- **deps:** update dependency eslint-plugin-jest to v26.1.4 a8eea3b
- **deps:** update dependency eslint-plugin-jest to v26.1.5 9065b1e
- **deps:** update dependency eslint-plugin-security to v1.5.0 7972341
- **deps:** update dependency eslint-plugin-sonarjs to v0.13.0 401fa51
- **deps:** update dependency eslint-plugin-tsdoc to v0.2.15 d61aa80
- **deps:** update dependency eslint-plugin-tsdoc to v0.2.16 6eb13ff
- **deps:** update dependency eslint-plugin-vue to v8.6.0 6d0a884
- **deps:** update dependency eslint-plugin-vue to v8.7.1 ebbc0b8
- **deps:** update typescript-eslint monorepo to v5.17.0 80fda8a
- **deps:** update typescript-eslint monorepo to v5.18.0 de6fdd1
- **deps:** update typescript-eslint monorepo to v5.19.0 356deef
- **deps:** update typescript-eslint monorepo to v5.20.0 3ef2692
- **deps:** update typescript-eslint monorepo to v5.21.0 afe3938

### 5.4.5 (2022-04-24)

### Dependency upgrades

- **deps:** update dependency @rushstack/eslint-patch to v1.1.2 68a908f
- **deps:** update dependency @rushstack/eslint-patch to v1.1.3 fa82fab
- **deps:** update dependency eslint to v8.13.0 49f8040
- **deps:** update dependency eslint to v8.14.0 060faff
- **deps:** update dependency eslint-plugin-import to v2.26.0 dbea9bb
- **deps:** update dependency eslint-plugin-jest to v26.1.4 a8eea3b
- **deps:** update dependency eslint-plugin-jest to v26.1.5 9065b1e
- **deps:** update dependency eslint-plugin-security to v1.5.0 7972341
- **deps:** update dependency eslint-plugin-sonarjs to v0.13.0 401fa51
- **deps:** update dependency eslint-plugin-tsdoc to v0.2.15 d61aa80
- **deps:** update dependency eslint-plugin-tsdoc to v0.2.16 6eb13ff
- **deps:** update dependency eslint-plugin-vue to v8.6.0 6d0a884
- **deps:** update dependency eslint-plugin-vue to v8.7.1 ebbc0b8
- **deps:** update typescript-eslint monorepo to v5.17.0 80fda8a
- **deps:** update typescript-eslint monorepo to v5.18.0 de6fdd1
- **deps:** update typescript-eslint monorepo to v5.19.0 356deef
- **deps:** update typescript-eslint monorepo to v5.20.0 3ef2692

### 5.4.5 (2022-04-17)

### Dependency upgrades

- **deps:** update dependency @rushstack/eslint-patch to v1.1.2 68a908f
- **deps:** update dependency @rushstack/eslint-patch to v1.1.3 fa82fab
- **deps:** update dependency eslint to v8.13.0 49f8040
- **deps:** update dependency eslint-plugin-import to v2.26.0 dbea9bb
- **deps:** update dependency eslint-plugin-jest to v26.1.4 a8eea3b
- **deps:** update dependency eslint-plugin-sonarjs to v0.13.0 401fa51
- **deps:** update dependency eslint-plugin-tsdoc to v0.2.15 d61aa80
- **deps:** update dependency eslint-plugin-tsdoc to v0.2.16 6eb13ff
- **deps:** update dependency eslint-plugin-vue to v8.6.0 6d0a884
- **deps:** update typescript-eslint monorepo to v5.17.0 80fda8a
- **deps:** update typescript-eslint monorepo to v5.18.0 de6fdd1
- **deps:** update typescript-eslint monorepo to v5.19.0 356deef

### 5.4.5 (2022-04-03)

### Dependency upgrades

- **deps:** update dependency eslint-plugin-sonarjs to v0.13.0 401fa51
- **deps:** update typescript-eslint monorepo to v5.17.0 80fda8a

### 5.4.4 (2022-03-27)

### Dependency upgrades

- **deps:** update dependency eslint to v8.12.0 96babe2
- **deps:** update dependency eslint-plugin-jest to v26.1.3 8c4d0e3

### 5.4.3 (2022-03-25)

### Bug Fixes

- **@html-validate/eslint-config-jest, @html-validate/eslint-config:** disable typeinfo rules for jest entirely e77c5d6

### 5.4.2 (2022-03-25)

### Bug Fixes

- **@html-validate/eslint-config-jest:** dont check unsafe assignment to any in test-cases 7127551
- **@html-validate/eslint-config:** only apply typeinfo rules on src folder c9e0338

### 5.4.1 (2022-03-24)

### Bug Fixes

- **@html-validate/eslint-config-jest:** disable `@typescript-eslint/no-non-null-assertion` for test-cases e1df9d1
- **@html-validate/eslint-config-typescript-typeinfo:** disable `@typescript-eslint/no-unsafe-member-access` 73cdf3f
- **@html-validate/eslint-config-typescript-typeinfo:** enable `@typescript-eslint/consistent-type-definitions` 6b3ef26
- **@html-validate/eslint-config-typescript-typeinfo:** prefer to use inline `type` c356a9a

## 5.4.0 (2022-03-24)

### Features

- **@html-validate/eslint-config-typescript-typeinfo, @html-validate/eslint-config:** new preset `@html-validate/eslint-config-typescript-typeinfo` a04c24a

### Bug Fixes

- **integration-test, @html-validate/eslint-config:** set root property to true d2ca8b8

### Dependency upgrades

- **deps:** update dependency eslint-plugin-jest to v26.1.2 9bf008f
- **deps:** update typescript-eslint monorepo to v5.16.0 faf0b38

### 5.3.12 (2022-03-20)

### Dependency upgrades

- **deps:** update dependency @rushstack/eslint-patch to v1.1.1 1955ea0
- **deps:** update typescript-eslint monorepo to v5.15.0 9e52168

### 5.3.11 (2022-03-13)

### Dependency upgrades

- **deps:** update dependency eslint to v8.11.0 d0941ff

### 5.3.10 (2022-03-09)

### Bug Fixes

- **@html-validate/eslint-config-typescript:** disable `consistent-return` for typescript 4d37561

### Dependency upgrades

- **deps:** update typescript-eslint monorepo to v5.14.0 1e9c55c

### 5.3.9 (2022-03-06)

### Dependency upgrades

- **deps:** update dependency eslint-config-prettier to v8.5.0 5420bc2
- **deps:** update typescript-eslint monorepo to v5.13.0 a290ae4

### 5.3.8 (2022-02-27)

### Dependency upgrades

- **deps:** update dependency eslint to v8.10.0 43592bc
- **deps:** update dependency eslint-plugin-vue to v8.5.0 5566c1a
- **deps:** update typescript-eslint monorepo to v5.12.1 20f91c8

### 5.3.7 (2022-02-21)

### Bug Fixes

- **@html-validate/eslint-config:** disable `sonarjs/prefer-single-boolean-return` da61473

### Dependency upgrades

- **deps:** update dependency eslint-config-prettier to v8.4.0 4059e9c

### 5.3.6 (2022-02-20)

### Dependency upgrades

- **deps:** update dependency eslint-plugin-jest to v26.1.1 84c4048
- **deps:** update dependency eslint-plugin-sonarjs to v0.12.0 8f73f76

### 5.3.5 (2022-02-15)

### Dependency upgrades

- **deps:** update typescript-eslint monorepo to v5.12.0 38c6ad8

### 5.3.4 (2022-02-13)

### Dependency upgrades

- **deps:** update dependency eslint to v8.9.0 18b736d
- **deps:** update dependency eslint-plugin-jest to v26.1.0 4abfeea
- **deps:** update typescript-eslint monorepo to v5.11.0 b741cf8

### 5.3.3 (2022-02-07)

### Bug Fixes

- **@html-validate/eslint-config:** proper exit code in wrapped binary 368bd45

### Dependency upgrades

- **deps:** update dependency eslint-plugin-vue to v8.4.1 ba5370a

### 5.3.2 (2022-02-06)

### Dependency upgrades

- **deps:** update dependency eslint to v8.8.0 2aa11db
- **deps:** update dependency eslint-plugin-jest to v26 93abe8c
- **deps:** update dependency eslint-plugin-vue to v8.4.0 2b9d35b
- **deps:** update typescript-eslint monorepo to v5.10.2 6e46edd

### 5.3.1 (2022-01-17)

### Dependency upgrades

- **deps:** update dependency eslint to v8.7.0 0349e00
- **deps:** update dependency eslint-plugin-jest to v25.7.0 3e622b0
- **deps:** update dependency eslint-plugin-vue to v8.3.0 ec6da0c
- **deps:** update typescript-eslint monorepo to v5.9.1 9059808

## 5.3.0 (2022-01-06)

### Features

- **deps:** bundle eslint-config binary dependencies e428b42

### Dependency upgrades

- **deps:** update dependency eslint to v8.6.0 91acab0
- **deps:** update dependency eslint-plugin-import to v2.25.4 78fd1ff
- **deps:** update dependency eslint-plugin-jest to v25.3.4 aaf1b87
- **deps:** update dependency eslint-plugin-vue to v8.2.0 0d09a77
- **deps:** update typescript-eslint monorepo to v5.9.0 875b1e8

### 5.2.1 (2021-12-02)

### Bug Fixes

- **@html-validate/eslint-config:** spawn separate process as eslint binary is not exported under esm 669b18c

## 5.2.0 (2021-12-02)

### Features

- **@html-validate/eslint-config-angularjs, @html-validate/eslint-config-cypress, @html-validate/eslint-config-jest, @html-validate/eslint-config-protractor, @html-validate/eslint-config-typescript, @html-validate/eslint-config-vue, @html-validate/eslint-config:** drop support for node 10 6b96052

### Dependency upgrades

- **deps:** update dependency eslint to v8 880c951
- **deps:** update dependency eslint-plugin-jasmine to v4.1.3 9dbe4ad
- **deps:** update dependency eslint-plugin-jest to v25.3.0 cbf3245
- **deps:** update dependency eslint-plugin-sonarjs to v0.11.0 41c9909
- **deps:** update dependency eslint-plugin-vue to v8.1.1 36469ba
- **deps:** update typescript-eslint monorepo to v5.5.0 62be8ec

### 5.1.1 (2021-11-15)

### Bug Fixes

- **@html-validate/eslint-config:** json should always use extension 6c2ee72

## 5.1.0 (2021-11-15)

### Features

- **@html-validate/eslint-config:** wrap eslint binary and pull as regular dependency 4d25c69

### Dependency upgrades

- **deps:** pin dependencies 5c84c20
- **deps:** update dependency eslint-config-prettier to v8.3.0 f1310bb
- **deps:** update dependency eslint-plugin-angular to v4.1.0 b8ded5a
- **deps:** update dependency eslint-plugin-cypress to v2.12.1 1a51888
- **deps:** update dependency eslint-plugin-import to v2.25.3 01b42fa
- **deps:** update dependency eslint-plugin-jest to v25.2.4 71f7707
- **deps:** update typescript-eslint monorepo to v5.3.1 8b21849

## 5.0.0 (2021-11-13)

### ⚠ BREAKING CHANGES

- **@html-validate/eslint-config:** the configuration format has changed from `.eslintrc.json` to
  `.eslintrc.js`. Use `npm exec eslint-config -- --write` or manually migrate config.

### Features

- **@html-validate/eslint-config:** uses `@rushstack/eslint-patch` to workaround plugin loading issue 946889c

### 4.4.12 (2021-11-12)

### Dependency upgrades

- **deps:** update dependency eslint-formatter-gitlab to v3 42407c2
- **deps:** update dependency eslint-plugin-vue to v8 7a264a7

### 4.4.11 (2021-10-24)

### Bug Fixes

- **@html-validate/eslint-config:** some autodetection typos d45d507

### 4.4.10 (2021-10-16)

### Bug Fixes

- **@html-validate/eslint-config:** use resolve.resolve to resolve extended config c7a36ac

### Dependency upgrades

- **deps:** update dependency eslint-plugin-jest to v25 400e804
- **deps:** update typescript-eslint monorepo to v5 3301b87

### 4.4.9 (2021-10-10)

### Bug Fixes

- **@html-validate/eslint-config-cypress:** disable `sonarjs/no-duplicate-string` for cypress acef611

### 4.4.8 (2021-09-12)

### Bug Fixes

- **@html-validate/eslint-config:** revert `find-up` to v5 a10c262

### 4.4.7 (2021-09-12)

### Dependency upgrades

- **deps:** update dependency find-up to v6 62ba783

### 4.4.6 (2021-09-05)

### Dependency upgrades

- **deps:** update dependency eslint-plugin-prettier to v4 bd6a3ac

### 4.4.5 (2021-08-15)

### Dependency upgrades

- **deps:** update dependency eslint-plugin-sonarjs to ^0.10.0 cf43f65

### 4.4.4 (2021-07-11)

### Dependency upgrades

- **deps:** update dependency eslint-plugin-sonarjs to ^0.9.0 da02c67

### 4.4.3 (2021-06-20)

### Bug Fixes

- **@html-validate/eslint-config:** disable `sonarjs/no-small-switch` 74b14d7

### 4.4.2 (2021-05-29)

### Bug Fixes

- **@html-validate/eslint-config-jest:** disable `tsdoc/syntax` for test case 6305eb1

### 4.4.1 (2021-05-23)

### Bug Fixes

- **@html-validate/eslint-config-jest, @html-validate/eslint-config:** adjust security rules a bit e3d5c25

## 4.4.0 (2021-05-08)

### Features

- **@html-validate/eslint-config:** bump some rules to errors a2bf74d

### Bug Fixes

- **jest:** disable sonarjs/no-duplicate-string 0516705

## [4.3.0](https://gitlab.com/html-validate/eslint-config/compare/v4.2.0...v4.3.0) (2021-05-02)

### Features

- **@html-validate/eslint-config:** use eslint-plugin-security ([0f723f0](https://gitlab.com/html-validate/eslint-config/commit/0f723f068f8164715656544d76a0708c24056662))
- **@html-validate/eslint-config:** use eslint-plugin-sonarjs ([4ae317f](https://gitlab.com/html-validate/eslint-config/commit/4ae317f7cde4f825f47c2d8746d2ddf8801231b3))

## [4.2.0](https://gitlab.com/html-validate/eslint-config/compare/v4.1.0...v4.2.0) (2021-04-27)

### Features

- **@html-validate/eslint-config-typescript:** use `eslint-plugin-tsdoc` for typescript ([77da74f](https://gitlab.com/html-validate/eslint-config/commit/77da74f1602f2299a68dd12e81edc66ab6c5a5e8))

## [4.1.0](https://gitlab.com/html-validate/eslint-config/compare/v4.0.0...v4.1.0) (2021-02-28)

### Features

- **@html-validate/eslint-config-angularjs, @html-validate/eslint-config:** add angularjs config ([02a3120](https://gitlab.com/html-validate/eslint-config/commit/02a312041aec03fb9db1f5c401b13484a7b93c0b))
- **@html-validate/eslint-config-protractor, @html-validate/eslint-config:** add protractor config ([393733c](https://gitlab.com/html-validate/eslint-config/commit/393733c2cce749d41a831fce60254721a5e456ae))
- **@html-validate/eslint-config-vue, @html-validate/eslint-config:** add vue config ([d2455af](https://gitlab.com/html-validate/eslint-config/commit/d2455af1b68d607ed0d6d079d2bd85e1baff2d68))

## [4.0.0](https://gitlab.com/html-validate/eslint-config/compare/v3.2.0...v4.0.0) (2021-02-28)

### ⚠ BREAKING CHANGES

- **deps:** Update dependency eslint-config-prettier to v8

### Dependency upgrades

- **deps:** Update dependency eslint-config-prettier to v8 ([ec03e87](https://gitlab.com/html-validate/eslint-config/commit/ec03e8766dbc1683183037aa47507db648da1e38))

## [3.2.0](https://gitlab.com/html-validate/eslint-config/compare/v3.1.0...v3.2.0) (2021-02-14)

### Features

- **@html-validate/eslint-config:** include `eslint-formatter-gitlab` ([2223e25](https://gitlab.com/html-validate/eslint-config/commit/2223e25add1e592b79b69c4bbe8d41af093a9d01))

## [3.1.0](https://gitlab.com/html-validate/eslint-config/compare/v3.0.1...v3.1.0) (2020-12-19)

### Features

- **@html-validate/eslint-config:** `--write` will install presets as needed ([1c44910](https://gitlab.com/html-validate/eslint-config/commit/1c4491023e165595e4a94222da92ce1d64763d19))

### [3.0.1](https://gitlab.com/html-validate/eslint-config/compare/v3.0.0...v3.0.1) (2020-12-19)

### Bug Fixes

- **@html-validate/eslint-config:** add missing file ([75ede61](https://gitlab.com/html-validate/eslint-config/commit/75ede61c1a8ab1b6f0b9535bd98dff2487a4aa28))

## [3.0.0](https://gitlab.com/html-validate/eslint-config/compare/v2.3.2...v3.0.0) (2020-12-19)

### ⚠ BREAKING CHANGES

- **@html-validate/eslint-config-cypress, @html-validate/eslint-config-jest, @html-validate/eslint-config-typescript, @html-validate/eslint-config:** Previously the `@html-validate/eslint-config` contained all
  presets (jest, cypress, typescript) but those are now in separate packages. See
  README.md or use `eslint-config --write` to update configuration.
- **deps:** Update dependency eslint-config-prettier to v7

### Features

- **@html-validate/eslint-config-cypress, @html-validate/eslint-config-jest, @html-validate/eslint-config-typescript, @html-validate/eslint-config:** split into monorepo with multiple presets ([25d5aff](https://gitlab.com/html-validate/eslint-config/commit/25d5aff93221d14736790b47f2de848d3cc62bcf))

### Bug Fixes

- update `typescript` rules to match `eslint-config-prettier` ([1d589f0](https://gitlab.com/html-validate/eslint-config/commit/1d589f0ae2bb13efcb08cd9ba8f70b76109c7602))

### Dependency upgrades

- **deps:** Update dependency eslint-config-prettier to v7 ([d47913c](https://gitlab.com/html-validate/eslint-config/commit/d47913c24665a04d6868c6e7f41db037331fc909))

# @html-validate/eslint-config changelog

### [2.3.2](https://gitlab.com/html-validate/eslint-config/compare/v2.3.1...v2.3.2) (2020-11-23)

### Bug Fixes

- force `ecmaVersion` to 2019 ([997632e](https://gitlab.com/html-validate/eslint-config/commit/997632e846e365e05117e307301d4659e7f55ed6))

### Dependency upgrades

- **deps:** update dependency eslint-plugin-cypress to v2.11.2 ([f5d998b](https://gitlab.com/html-validate/eslint-config/commit/f5d998b1713ffa151eb296ccd6a32858ec49fc74))

### [2.3.1](https://gitlab.com/html-validate/eslint-config/compare/v2.3.0...v2.3.1) (2020-11-22)

### Bug Fixes

- cypress env global ([202acf3](https://gitlab.com/html-validate/eslint-config/commit/202acf3c211aadc473f958ebaa6fba1c2bd9331f))

## [2.3.0](https://gitlab.com/html-validate/eslint-config/compare/v2.2.0...v2.3.0) (2020-11-22)

### Features

- cypress preset ([72b08a0](https://gitlab.com/html-validate/eslint-config/commit/72b08a0dfad380ae3f733df0e3d60bfae731ef0f))

### Bug Fixes

- exclude cypress from jest config ([8af47d7](https://gitlab.com/html-validate/eslint-config/commit/8af47d7f5c910c5ffc175849bbb51d5803668baf))
- force ignore coverage and public assets ([dd7237e](https://gitlab.com/html-validate/eslint-config/commit/dd7237e9b68b681b297926f2fbb471c8da9c6a80))

## [2.2.0](https://gitlab.com/html-validate/eslint-config/compare/v2.1.1...v2.2.0) (2020-11-21)

### Features

- use `eslint-plugin-array-func` ([d78926a](https://gitlab.com/html-validate/eslint-config/commit/d78926ae302820472594df98578db455b990f10f))

### [2.1.1](https://gitlab.com/html-validate/eslint-config/compare/v2.1.0...v2.1.1) (2020-11-09)

### Bug Fixes

- dont check for bundled deps ([16c1e7a](https://gitlab.com/html-validate/eslint-config/commit/16c1e7af73dcad04537cc44f765c19fee34a07f3))

## [2.1.0](https://gitlab.com/html-validate/eslint-config/compare/v2.0.0...v2.1.0) (2020-11-08)

### Features

- `--write` will install dependencies ([238bc3b](https://gitlab.com/html-validate/eslint-config/commit/238bc3bb48ec9baec391db9911c4a52f4dc5177c))

### Bug Fixes

- dont include tests in npm package ([66f986d](https://gitlab.com/html-validate/eslint-config/commit/66f986d2152e05db2a9bd43c8990e3ad15971678))

## [2.0.0](https://gitlab.com/html-validate/eslint-config/compare/v1.5.25...v2.0.0) (2020-11-01)

### ⚠ BREAKING CHANGES

- packages must now put compiled files into the `dist` folder

### Features

- use `dist` folder instead of `build` ([491a1ff](https://gitlab.com/html-validate/eslint-config/commit/491a1ff35351383b6508dbce17de2b967cb233c1))

## [1.5.25](https://gitlab.com/html-validate/eslint-config/compare/v1.5.24...v1.5.25) (2020-11-01)

## [1.5.24](https://gitlab.com/html-validate/eslint-config/compare/v1.5.23...v1.5.24) (2020-10-25)

## [1.5.23](https://gitlab.com/html-validate/eslint-config/compare/v1.5.22...v1.5.23) (2020-10-18)

## [1.5.22](https://gitlab.com/html-validate/eslint-config/compare/v1.5.21...v1.5.22) (2020-10-11)

### Bug Fixes

- **deps:** update dependency argparse to v2 ([fb7ccc0](https://gitlab.com/html-validate/eslint-config/commit/fb7ccc03511adac4f46ce5e51e110653feefa481))

## [1.5.21](https://gitlab.com/html-validate/eslint-config/compare/v1.5.20...v1.5.21) (2020-10-04)

## [1.5.20](https://gitlab.com/html-validate/eslint-config/compare/v1.5.19...v1.5.20) (2020-09-27)

## [1.5.19](https://gitlab.com/html-validate/eslint-config/compare/v1.5.18...v1.5.19) (2020-09-20)

### Bug Fixes

- **deps:** update dependency eslint-plugin-jest to v24 ([c0b550a](https://gitlab.com/html-validate/eslint-config/commit/c0b550aca29e884d99154cfd7d5a9914a3dbd6b2))
- **deps:** update typescript-eslint monorepo to v4 ([b103c80](https://gitlab.com/html-validate/eslint-config/commit/b103c804e65a74ec59ad2cbe4f9b1576ef8930d0))

## [1.5.18](https://gitlab.com/html-validate/eslint-config/compare/v1.5.17...v1.5.18) (2020-09-13)

## [1.5.17](https://gitlab.com/html-validate/eslint-config/compare/v1.5.16...v1.5.17) (2020-09-06)

## [1.5.16](https://gitlab.com/html-validate/eslint-config/compare/v1.5.15...v1.5.16) (2020-08-30)

## [1.5.15](https://gitlab.com/html-validate/eslint-config/compare/v1.5.14...v1.5.15) (2020-08-23)

### Bug Fixes

- **deps:** update dependency find-up to v5 ([83f1f31](https://gitlab.com/html-validate/eslint-config/commit/83f1f31efcb08eee4da741f71990c02148eae8c0))

## [1.5.14](https://gitlab.com/html-validate/eslint-config/compare/v1.5.13...v1.5.14) (2020-08-16)

## [1.5.13](https://gitlab.com/html-validate/eslint-config/compare/v1.5.12...v1.5.13) (2020-08-09)

## [1.5.12](https://gitlab.com/html-validate/eslint-config/compare/v1.5.11...v1.5.12) (2020-08-02)

## [1.5.11](https://gitlab.com/html-validate/eslint-config/compare/v1.5.10...v1.5.11) (2020-07-26)

### Bug Fixes

- **deps:** update dependency nunjucks to v3.2.2 ([b0c796d](https://gitlab.com/html-validate/eslint-config/commit/b0c796d9232936bcc4696f8683930baf7baebfb9))

## [1.5.10](https://gitlab.com/html-validate/eslint-config/compare/v1.5.9...v1.5.10) (2020-07-19)

## [1.5.9](https://gitlab.com/html-validate/eslint-config/compare/v1.5.8...v1.5.9) (2020-07-12)

## [1.5.8](https://gitlab.com/html-validate/eslint-config/compare/v1.5.7...v1.5.8) (2020-07-05)

## [1.5.7](https://gitlab.com/html-validate/eslint-config/compare/v1.5.6...v1.5.7) (2020-06-28)

## [1.5.6](https://gitlab.com/html-validate/eslint-config/compare/v1.5.5...v1.5.6) (2020-06-21)

## [1.5.5](https://gitlab.com/html-validate/eslint-config/compare/v1.5.4...v1.5.5) (2020-06-14)

### Bug Fixes

- **deps:** update typescript-eslint monorepo to v3 ([8d4db80](https://gitlab.com/html-validate/eslint-config/commit/8d4db80ab9048bd65078757d24ac396b5856f841))

## [1.5.4](https://gitlab.com/html-validate/eslint-config/compare/v1.5.3...v1.5.4) (2020-06-07)

## [1.5.3](https://gitlab.com/html-validate/eslint-config/compare/v1.5.2...v1.5.3) (2020-05-31)

## [1.5.2](https://gitlab.com/html-validate/eslint-config/compare/v1.5.1...v1.5.2) (2020-05-24)

## [1.5.1](https://gitlab.com/html-validate/eslint-config/compare/v1.5.0...v1.5.1) (2020-05-17)

### Bug Fixes

- add `eslint:fix` script ([0983c8f](https://gitlab.com/html-validate/eslint-config/commit/0983c8f8626e5aa9a9a0febd009ea534bada2e4c))

# [1.5.0](https://gitlab.com/html-validate/eslint-config/compare/v1.4.2...v1.5.0) (2020-05-10)

### Features

- **cli:** autodetect typescript and jest ([3ab7abf](https://gitlab.com/html-validate/eslint-config/commit/3ab7abf202abf08befa2a115dd17d68d7dd628cf))

## [1.4.2](https://gitlab.com/html-validate/eslint-config/compare/v1.4.1...v1.4.2) (2020-05-10)

### Bug Fixes

- **cli:** fix exit code ([71f7627](https://gitlab.com/html-validate/eslint-config/commit/71f7627a541015d59966ea9a82d032bd669ab3fd))

## [1.4.1](https://gitlab.com/html-validate/eslint-config/compare/v1.4.0...v1.4.1) (2020-05-10)

### Bug Fixes

- **cli:** fix shebang ([bda531b](https://gitlab.com/html-validate/eslint-config/commit/bda531bcc12ef72a3580ae632c8c5c7d109126ce))

# [1.4.0](https://gitlab.com/html-validate/eslint-config/compare/v1.3.3...v1.4.0) (2020-05-10)

### Features

- **cli:** cli script will update `package.json` with `eslint` script ([c4f85d8](https://gitlab.com/html-validate/eslint-config/commit/c4f85d8b598382a4ac6fd6eee1b09e44d722111f))

## [1.3.3](https://gitlab.com/html-validate/eslint-config/compare/v1.3.2...v1.3.3) (2020-05-03)

## [1.3.2](https://gitlab.com/html-validate/eslint-config/compare/v1.3.1...v1.3.2) (2020-04-26)

## [1.3.1](https://gitlab.com/html-validate/eslint-config/compare/v1.3.0...v1.3.1) (2020-04-20)

### Bug Fixes

- add missing file ([ab24e31](https://gitlab.com/html-validate/eslint-config/commit/ab24e31956d3f010fb0d5c8eba2004493aeace72))

# [1.3.0](https://gitlab.com/html-validate/eslint-config/compare/v1.2.4...v1.3.0) (2020-04-20)

### Features

- experimental support for `--apply` ([6c40a6f](https://gitlab.com/html-validate/eslint-config/commit/6c40a6f4723dcd07ee33a66ff1008c4be6f778d4))

## [1.2.4](https://gitlab.com/html-validate/eslint-config/compare/v1.2.3...v1.2.4) (2020-04-19)

## [1.2.3](https://gitlab.com/html-validate/eslint-config/compare/v1.2.2...v1.2.3) (2020-04-12)

## [1.2.2](https://gitlab.com/html-validate/eslint-config/compare/v1.2.1...v1.2.2) (2020-04-05)

## [1.2.1](https://gitlab.com/html-validate/eslint-config/compare/v1.2.0...v1.2.1) (2020-03-29)

# [1.2.0](https://gitlab.com/html-validate/eslint-config/compare/v1.1.4...v1.2.0) (2020-03-22)

### Features

- enable eslint-plugin-node ([95d198b](https://gitlab.com/html-validate/eslint-config/commit/95d198bfcf5fe983194a8096dcac9b5f4660e1b6))
- stricter jest rules ([df8fbad](https://gitlab.com/html-validate/eslint-config/commit/df8fbadc79ffb773705691cf23e7bae31cf8b8f6))

## [1.1.4](https://gitlab.com/html-validate/eslint-config/compare/v1.1.3...v1.1.4) (2020-03-01)

## [1.1.3](https://gitlab.com/html-validate/eslint-config/compare/v1.1.2...v1.1.3) (2020-02-23)

## [1.1.2](https://gitlab.com/html-validate/eslint-config/compare/v1.1.1...v1.1.2) (2020-02-16)

## [1.1.1](https://gitlab.com/html-validate/eslint-config/compare/v1.1.0...v1.1.1) (2020-02-13)

### Bug Fixes

- disable `import/default` ([84f6795](https://gitlab.com/html-validate/eslint-config/commit/84f6795fd553bd849f5efb570171fa51333e717f))

# [1.1.0](https://gitlab.com/html-validate/eslint-config/compare/v1.0.10...v1.1.0) (2020-02-11)

### Features

- add eslint-plugin-import ([8233469](https://gitlab.com/html-validate/eslint-config/commit/8233469928c1b12768383fc2c8dd83becebb7def))

## [1.0.10](https://gitlab.com/html-validate/eslint-config/compare/v1.0.9...v1.0.10) (2020-02-09)

## [1.0.9](https://gitlab.com/html-validate/eslint-config/compare/v1.0.8...v1.0.9) (2020-02-02)

## [1.0.8](https://gitlab.com/html-validate/eslint-config/compare/v1.0.7...v1.0.8) (2020-01-26)

## [1.0.7](https://gitlab.com/html-validate/eslint-config/compare/v1.0.6...v1.0.7) (2020-01-12)

## [1.0.6](https://gitlab.com/html-validate/eslint-config/compare/v1.0.5...v1.0.6) (2020-01-05)

## [1.0.5](https://gitlab.com/html-validate/eslint-config/compare/v1.0.4...v1.0.5) (2019-12-29)

## [1.0.4](https://gitlab.com/html-validate/eslint-config/compare/v1.0.3...v1.0.4) (2019-12-25)

### Bug Fixes

- add missing eslint-plugin-jest ([e0459f0](https://gitlab.com/html-validate/eslint-config/commit/e0459f044406530bf97ce7699522b492c9aefdbb))

## [1.0.3](https://gitlab.com/html-validate/eslint-config/compare/v1.0.2...v1.0.3) (2019-12-25)

### Bug Fixes

- back to using regular dependency ([b020a3e](https://gitlab.com/html-validate/eslint-config/commit/b020a3e01e7bff58d69b255c4f65c4af0945b0c1))

## [1.0.2](https://gitlab.com/html-validate/eslint-config/compare/v1.0.1...v1.0.2) (2019-12-25)

### Bug Fixes

- use peerDependency ([41879f2](https://gitlab.com/html-validate/eslint-config/commit/41879f216e2b6a5b9a6f96d7b70daaea2d47ba99))
- **deps:** update typescript-eslint monorepo to v2.13.0 ([de95d83](https://gitlab.com/html-validate/eslint-config/commit/de95d83cacbaa22b80272f427518c094c25e5dc3))

## [1.0.1](https://gitlab.com/html-validate/eslint-config/compare/v1.0.0...v1.0.1) (2019-12-24)

### Bug Fixes

- add missing files ([316fea4](https://gitlab.com/html-validate/eslint-config/commit/316fea448765b5189885f657ee11650db0544fa0))

# 1.0.0 (2019-12-24)

### Features

- initial release refactored from html-validate ([924fbd9](https://gitlab.com/html-validate/eslint-config/commit/924fbd9a5ba0bb460535cb920f970ab38661be14))
